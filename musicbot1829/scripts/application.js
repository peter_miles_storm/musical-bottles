var application = {
    onBodyLoad: function () {
		console.log("Application.onBodyLoad called");
        document.addEventListener("deviceready", application.onDeviceReady, false);
		
		application.onDeviceReady();
    },
    onDeviceReady: function () {
		console.log("Application.onDeviceReady called");
        document.addEventListener("orientationChanged", application.updateOrientation, false);
        // Prevent dragging:
        document.addEventListener("touchmove", application.preventScrolling, false);

        // detect orientation
        if (window.innerWidth < window.innerHeight) {
            application.changeOrientation(180);
        }

        // initialize sound
        //Application.playSound("sounds/no_sound.mp3");
        application.autoDetectScreenResolution();
		//MusicalBottles.onresize();
    },
    preventScrolling: function (e) {
        e.preventDefault();
    },
    // Warning: timeout needed to make it stable. Alert on rotation crashes the iPad!
    updateOrientation: function (e) {
        setTimeout(function () {
            application.changeOrientation(e.orientation);
        }, 0);
    },

	soundEnabled: true,
    playSound: function (src, secondSrc, endOfSoundCallback, repeat) {
		if (this.soundEnabled) {
			if (typeof device != "undefined") {
				var sound = new Media(src, endOfSoundCallback);
				if (! repeat) {
					sound.play();
				} else {
					sound.play({numberOfLoops:-1});
				}
				if (secondSrc != null) {
					setTimeout(function () {
						new Media(secondSrc, endOfSoundCallback).play();
					}, 600);
				}
				return sound;
			} else {
				var audio = document.createElement("audio");
				audio.src = src;
				if (repeat) {
					audio.loop = "loop";
				}
				audio.addEventListener("ended", function () {
					this.parentNode.removeChild(this);
					if (endOfSoundCallback != null) {
						endOfSoundCallback.call(this);
					}
				});
				document.body.appendChild(audio);
				audio.load();
				audio.play();

				if (secondSrc != null) {
					var audio2 = document.createElement("audio");
					audio2.src = secondSrc;
					audio2.addEventListener("ended", function () {
						this.parentNode.removeChild(this);
						if (endOfSoundCallback != null) {
							endOfSoundCallback.call(this);
						}
					});
					document.body.appendChild(audio2);
					audio2.load();
					audio2.play();
				}
				return audio;
			}
		}
    },
    pauseSound: function (soundObject) {
        if (soundObject != null) {
            soundObject.pause();
        }
    },
    unpauseSound: function (soundObject, repeat) {
        if (soundObject != null) {
            if (typeof device != "undefined") {
                if (! repeat) {
                    soundObject.play();
                } else {
                    soundObject.play({numberOfLoops:-1});
                }
            } else {
                if (! repeat) {
                    if (soundObject.loop != null) {
                        delete soundObject.loop;
                    }
                } else {
                    soundObject.loop = "loop";
                }
                soundObject.play();
            }
        }
    },
    openMagmentis: function () {
        top.location.href = "http://www.magmentis.com";
    },
    autoDetectScreenResolution: function () {
        if (document.getElementById("preferredStylesheet") == null) {
            var resolution = null;
            if (window.devicePixelRatio == 2) {
                resolution = "480x320-retina";
            } else {
                resolution = "480x320";
            }
            var link = document.createElement("link");
            link.setAttribute("rel", "stylesheet");
            link.setAttribute("href", "styles/" + resolution + ".css");
            document.body.appendChild(link);
        }
    },
    randomize: function (array) {
        if (array != null) {
            array.sort(function () {
                return (Math.round(Math.random()) - 0.5);
            });
        }
    },
	getTime: function() {
		var d = new Date();
		var n = d.getTime(); 
		return n;
	},	
   onresize: function() {
        	var d = document.getElementById("maindiv");
        	var w = 1024;
			
			MusicalBottles.MainMaxX = w;
        	//var h = 768;
			var h = 658; //768; // no longer sure what the advantage of 658 is ...
			if (MusicalBottles.isIpad)
			   h = 768;
        
   			var leftx = ""+ ((window.innerWidth - w) / 2) + "px";
			var topy = 0;
			if (window.innerHeight > h) {
				topy = ""+ ((window.innerHeight - h) / 2) + "px";
			} else {
				topy ="0px";
			}
			//console.log("on resize "+leftx);
			MusicalBottles.LeftX = ((window.innerWidth - w) / 2);
			MusicalBottles.TopY = ((window.innerHeight - h) / 2);
   			d.style.left = leftx;
   			d.style.top = topy;
	},
// Utility function - deep copying of objects
    extend: function (from, to)
{
    if (from == null || typeof from != "object") return from;
    if (from.constructor != Object && from.constructor != Array) return from;
    if (from.constructor == Date || from.constructor == RegExp || from.constructor == Function ||
        from.constructor == String || from.constructor == Number || from.constructor == Boolean)
        return new from.constructor(from);

    to = to || new from.constructor();

    for (var name in from)
    {
        to[name] = typeof to[name] == "undefined" ? application.extend(from[name], null) : to[name];
    }

    return to;
}	
};