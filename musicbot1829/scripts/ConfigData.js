var cfg = {
	topics: [
		{
			title: "start",					// Text shown on button
			sound: "bottles",
			help: "help_bottles",
            file: "background768.png", //"background768.jpg",
            //fgfile: "foreground768.png",
			HomeIconCoords: {
				left: 672,
				top: 105,
				width: 325,
				height: 70,
			},
            buttons: [
                {
                    name: "add_bottle",
                    down_img: "add2.png",
                    up_img: "add1.png",
                    grey_img: "add1_grey.png",
					downTime: 0,
                    rect: {
                        left: 960,
                        top: 460,
                        width: 68,
                        height: 66,
                    },
                },
                {
                    name: "remove_bottle",
                    down_img: "remove2.png",
                    up_img: "remove1.png",
                    grey_img: "remove1_grey.png",					
                    rect: {
                        left: 960,
                        top: 530, //693,
                        width: 68,
                        height: 66,
                    },
                },
                {
                    name: "empty_bottle",
                    down_img: "minus_water2.png",
                    up_img: "minus_water1.png",
                    grey_img: "minus_grey2.png", //"minus_water1.png",					
                    rect: {
                        left: 945,
                        top: 0,
                        width: 50,
                        height: 50,
                    },
                },
                {
                    name: "back",
                    down_img: "backdark2.png",
                    up_img: "backdark1.png",
                    grey_img: "backdark.png",					
                    rect: {
                        left: 16,
                        top: 684,
                        width: 50,
                        height: 50,
                    },
                },				
                 {
                    name: "play",
                    down_img: "play2.png",
                    up_img: "play1.png",
                    grey_img: "play1_grey.png",					
                    rect: {
                        left: 830,
                        top: 460, //693,
                        width: 68,
                        height: 66,
                    },
                },
                {
                    name: "pause",
                    down_img: "pause2.png",
                    up_img: "pause1.png",
                    grey_img: "pause1_grey.png",					
                    rect: {
                        left: 895,
                        top: 460, //693,
                        width: 68,
                        height: 66,
                    },
                },
                 {
                    name: "record",
                    down_img: "record2.png",
                    up_img: "record1.png",
                    grey_img: "record1_grey.png",					
                    rect: {
                        left: 895,
                        top: 530, //693,
                        width: 68,
                        height: 66,
                    },
                },
                {
                    name: "stop",
                    down_img: "stop2c.png",
                    up_img: "stop1c.png",
                    grey_img: "stop1c_grey2.png",					
                    rect: {
                        left: 830,
                        top: 530, //693,
                        width: 68,
                        height: 66,
                    },
                }
				
            ],
			items: [
			 	{
		 		name: "bottle0",							//name of item 
		 		file: "b0.png",						// Image src
				sound: "bottle974",
				note_height: 30,
				wavelength_scale: 10,
                },
                {
		 		name: "bottle1",							//name of item 
		 		file: "b1.png",						// Image src
				sound: "bottle1093",   
				note_height: 36,
				wavelength_scale: 11,
                },
                {
		 		name: "bottle2",							//name of item 
		 		file: "b2.png",						// Image src
				sound: "bottle1226",
				note_height: 41,
				wavelength_scale: 12,
                },
                {
		 		name: "bottle3",							//name of item 
		 		file: "b3.png",						// Image src
				sound: "bottle1377",
				note_height: 47,
				wavelength_scale: 13,
                },
                {
		 		name: "bottle4",							//name of item 
		 		file: "b4.png",						// Image src
				sound: "bottle1545",
				note_height: 54,
				wavelength_scale: 14,
                },
                {
		 		name: "bottle5",							//name of item 
		 		file: "b5.png",						// Image src
				sound: "bottle1734",
				note_height: 60,
				wavelength_scale: 15,
                },
                {
		 		name: "bottle6",							//name of item 
		 		file: "b6.png",						// Image src
				sound: "bottle2063",
				note_height: 67,
				wavelength_scale: 15,
                },
                {
		 		name: "bottle7",							//name of item 
		 		file: "b7.png",						// Image src
				sound: "bottle2315",
				note_height: 74,
				wavelength_scale: 17,
                },
                {
                name: "bottle8",							//name of item 
		 		file: "b8.png",						// Image src
				sound: "bottle2573", 
				note_height: 81,				
				wavelength_scale: 18,
                },
                {
                name: "bottle9",							//name of item 
		 		file: "b9.png",						// Image src
				sound: "bottle2917", 
				note_height: 88,				
				wavelength_scale: 18,
                },
                {
                name: "bottle10",							//name of item 
		 		file: "b10.png",						// Image src
				sound: "bottle3090", 
				note_height: 95,				
				wavelength_scale: 19,
                },
                {
                name: "bottle11",							//name of item 
		 		file: "b11.png",						// Image src
				sound: "bottle3274", 
				note_height: 101,				
				wavelength_scale: 20,
                },
				{
                name: "bottle12",							//name of item 
		 		file: "b12.png",						// Image src
				sound: "bottle3469", 
				note_height: 107,				
				wavelength_scale: 21,
                },
                {
                name: "jug",
                file: "jug0.png",
                animation: [ "jug0.png", "jug1.png", "jug2.png", "jug3.png", "jug4.png" ],
                anim_index: 0,
                sound: "pour",
                rect: {
                    left: 100,
                    top:   20,
                    width: 188,
                    height: 216,
                },
                },
                {
                    name: "smudge",
                    file: "dress_me.png",
                    animation: [ "jug0.png", "jug1.png", "jug2.png", "jug3.png", "jug4.png" ],
                    anim_index: 0,
					upTime: 2000, // Smudge time spent up behind bottle in MS
                    interval: null,
                    sound: "pour",
                    maxTop: 460,
                    minTop: 300,
                    rect: {
                    left: 351,
                    top: 460,
                    width: 270,
                    height: 300,
                },
                },
		 	],
			reports: [
				{
					name: "notes_played",
				},
				{
					name: "tunes_recorded",
				},			
				{
					name: "tunes_played",
				},

				{
					name: "duration",
				}				
		 	]	 	
		},
		{ // second topic - empty bottle, as one per bottle
            buttons: [  
				{   name: "unused1",
				},
				{   name: "unused2",
				},
				{
                    name: "empty_bottle",
                    down_img: "empty_down.png",
                    up_img: "empty_up.png",
                    grey_img: "empty_up.png",					
                    rect: {
                        left: 945,
                        top: 0,
                        width: 50,
                        height: 50,
                    },
                }
			],
			items: [
			]
		}
	],
			
	getNumberTopics: function () {
		return 1;
	},
		
	itemIndexReturn: function(value, currentTopic){    //return the index of element in items array
		var i;
		var index = -1;
		for (i=0; i<currentTopic.items.length; i++){
			var name = currentTopic.items[i].name;
			if (name == value) {
				index = i;
				break;
				}			
			}		
		return index;
		},
		
	labelIndexReturn: function(value, currentTopic){    //return the index of element in items array
		var i;
		var index = -1;
		for (i=0; i<currentTopic.labels.length; i++){
			var name = currentTopic.labels[i].name;
			if (name == value) {
				index = i;
				break;
				}			
			}		
		return index;
		},
		
	topicIndexReturn: function(value){    //return the index of element in topics array
		var i;
		var index = -1;
		for (i=0; i<cfg.topics.length; i++){
			var name = cfg.topics[i].title;
			if (name === value) {
				index = i;
				break;
				}			
			}		
		return index;
	}
	
};


HomescreenClickZones = {
	instructions: {
        text: "instructions",left: 672,	top: 205,	width: 333,	height: 74,	audio: "bottle1031",	offset: 1 			// this is the 'number' for this control (comes from arbitary order of control[]!)
	},
	
	options: {
        text: "options",	left: 672,	top: 405,	width: 333,	height: 74,	audio: "bottle1158",	offset: 4
	},
	
	reports: {
        text: "reporting",	left: 672,	top: 305,	width: 333,	height: 74,	audio: "bottle1093",	offset: 2
	},
	
    start: {
        text: "start",		left: 672,	top: 105,	width: 333,	height: 74,	audio: "bottle974",		offset: 0
	},
	// Start is under "Topics" at the top of this file
};
