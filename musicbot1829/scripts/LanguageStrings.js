var LanguageStrings = {
    CurrentLocale: "en_GB",
	locales: ["en_GB", "es_ES"],
	
    getDeviceLocale: function() {
		var l;
		if (navigator.userLanguage) {// Explorer
			l = navigator.userLanguage;
		} else if (navigator.language) { // FF
			l = navigator.language;
		} else{
			l = "en_GB";
		}
		// temporary
		//l = "es_mx";
		
		l = l.substring(0, 2);
		
		var localstr = "";
		
		switch (l)
		{
			case 'fr':
				localstr = "fr_FR";
				break;
				
			case 'it':
				localstr = "it_IT";
				break;
				
			case 'es':
				localstr = "es_ES";
				break;
				
			case 'pt':
				localstr = "pt_BR";
				break;
				
			default:
				return("en_GB");
				break;
		}
	
		return localstr;
    },

    getLocale: function() {
	    // returns the locale for this device
		return LanguageStrings.CurrentLocale;
    },
		
	setLocale: function(locale) {
		if (locale === undefined) {
			locale = LanguageStrings.getDeviceLocale();
		}
		LanguageStrings.CurrentLocale = locale;
	},
		
	getString: function(whichString) {
		var test_exists = LanguageStrings.TheStrings[whichString];
		if (test_exists === undefined) return "{ "+whichString+" }";

		var s = LanguageStrings.TheStrings[whichString][LanguageStrings.CurrentLocale];
		return s;
	},

//////////////////////////////////////////////////////////////////////////////////////

	TheStrings: {
		
		place: {
			en_GB: "Tap the bottles to make music",
			es_ES: "Toca las botellas para hacer música"
		},
		sure: {
			en_GB: "Are you sure this item is placed correctly?",
			es_ES:  "¿Estás seguro de que este elemento se coloca correctamente?"
		},
		reports: {
			en_GB: "Reports",
			es_ES: "Informes"
		},
        options: {
            en_GB: "Options",
			es_ES: "Opciones"
        },
		instructions: {
			en_GB: "Instructions",
			es_ES: "Instrucciones"
		},
		start: {
			en_GB: "Start",
			es_ES: "Comienzo"
		},	
		resetreports: {
			en_GB: "Reset",
			es_ES: "Reiniciar"
		},
		en: {
			en_GB: "English",
			es_ES: "Inglés"
		},
		es: {
			en_GB: "Spanish",
			es_ES: "Español"
		},
		title: {
			en_GB: "Musical Bottles",
			es_ES: "Botellas musicales"
		},
		language: {
			en_GB: "Language",
			es_ES: "Idioma"
		},
		difficulty: {
			en_GB: "Level",
			es_ES: "Nivel"
		},
		audio: {
			en_GB: "Sound",
			es_ES: "Sonido"
		},
		easy: {
			en_GB: "Easy",
			es_ES: "Fácil"
		},
		medium: {
			en_GB: "Medium",
			es_ES: "Medio"
		},
		hard: {
			en_GB: "Hard",
			es_ES: "Dificil"
		},
		on: {
			en_GB: "On",
			es_ES: "Encendido"
		},
		off: {
			en_GB: "Off",
			es_ES: "Apagado"
		},
		show: {
			en_GB: "Show",
			es_ES: "Displayo"
		},
		prompt: {
			en_GB: "Show prompt",
			es_ES: "Show prompto"
		},		
		dontshow: {
			en_GB: "Don't show",
			es_ES: "No muestran"
		},
		sinewave: {
			en_GB: "Sound waves",
			es_ES: "Ondas sonoras"
		},
		playnote: {
			en_GB: "Musical notes",
			es_ES: "Notas musicales"
		},		
		recording0: {
			en_GB: "R E C O R D I N G",
			es_ES: "G R A B A C I Ó N"
		},	
		recording1: {
			en_GB: "I N",
			es_ES: "E N"
		},
		recording2: {
			en_GB: "P R O G R E S S",
			es_ES: "P R O G R E S O"
		},	
		tunes_recorded: {
			en_GB: "Tunes recorded",
			es_ES: "Sonidos grabados"
		},	
		tunes_played: {
			en_GB: "Tunes played",
			es_ES: "Sonidos reproducidos"
		},
		notes_played: {
			en_GB: "Notes played",
			es_ES: "Notas reproducidas"
		},
		duration: {
			en_GB: "Duration (minutes:seconds)",
			es_ES: "Duración (minutes:seconds)"
		},
		prompt_stoprec: {
			en_GB: "Press the button with the red square to stop the recording",
			es_ES: "Presiona el botón con el cuadrado rojo para detener la grabación"
		},
		prompt_clickplay: {
			en_GB: "You can play along by clicking on the bottles!",
			es_ES: "¡También puedes reproducir haciendo clic en las botellas!"
		},
		prompt_emptybottle: {
			en_GB: "You can empty the bottle at any time",
			es_ES: "Puedes vaciar la botella en cualquier momento"
		},
		prompt_rtorecord: {
			en_GB: "Press the button with the red circle to record the tune you create",
			es_ES: "Presiona el botón con el círculo rojo para grabar la melodía que creas"
		},
		prompt_addbottles: {
			en_GB: "You can add more bottles by pressing the button with + on it",
			es_ES: "Puedes añadir más botellas presionando el botón +"
		},
		prompt_fillwater: {
			en_GB: "Try filling the bottle with some water",
			es_ES: "Intenta llenar la botella con un poco de agua"
		},
		prompt_clicksound: {
			en_GB: "Click the mouse on the bottle to hear the sound it makes",
			es_ES: "Haz clic en la botella para oir el sonido que hace"
		},		
		instruction_text: {
			en_GB: "Initially, a single bottle is shown in the middle of the screen. If the mouse is moved near the bottle, a drum stick will appear, and clicking the mouse will cause the drum stick to tap the bottle, and the sound of the bottle will be heard. The sound made by tapping the bottle may be changed by filling or removing water from the bottle.%%To fill a bottle, drag the jug to a position above a bottle, and the jug will start to tilt and fill up the bottle. When the jug is moved away from the bottle, or if the bottle is full, the jug will stop pouring. Underneath the bottle is a button, which can be used to empty the contents of the bottle.%%Bottles may be added or removed using the buttons on the right of the screen. If you have several bottles, different amounts of water can be put into each of them, and they will make different sounds when they are tapped and children can play a simple tune, by tapping on the different bottles!%%For a bit of fun, the tune made can be recorded and played back. To do this: click on the button called 'start recording', tap on the bottles to make a tune, and then click the 'stop recording’ button. To play back the tune, click on the play button. When playing back, you can still tap the bottles to add to the tune!",
			es_ES: "Inicialmente, se muestra una sola botella en el centro de la pantalla. Si el ratón se mueve cerca de la botella, aparecerá un palo de tambor, y haciendo clic en el ratón hará que el palo de tambor golpee a la botella, y se escuchará el sonido. Este sonido puede cambiarse llenando o vaciando el agua de la botella.%%Para llenar una botella, arrastre la jarra a una posición por encima de una botella, y la jarra comenzará a inclinarse y a llenar la botella. Cuando la jarra se aleja de la botella, o si la botella está llena, la jarra dejará de verter agua. Debajo de la botella hay un botón que se puede utilizar para vaciar el contenido de la botella.%%Se pueden añadir o eliminar botellas utilizando los botones de la derecha de la pantalla. Si tienes varias botellas, se puede verter diferentes cantidades de agua en cada una de ellas, y harán distintos sonidos cuando se les golpee. ¡Los niños podrán tocar una melodía simple pulsando sobre las distintas botellas!%%Para un poco de diversión, la melodía hecha se puede grabar y reproducir. Para ello, haga clic en el botón denominado 'empezar a grabar', toque en las botellas para hacer una canción y, a continuación, haga clic en el botón ‘detener la grabación'. Para reproducir la canción, haga clic en el botón de reproducción. ¡Cuando se reproduzca todavía podrás tocar las botellas para añadir notas a la melodía!"
		}	
	}
};
