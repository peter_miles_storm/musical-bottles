
// Strict is good
"use strict";

/*
 *	Options store to keep options related common functionality and options that are pretty app generic
 */ 
var options = {
	horizontalSwitch: false,
	
	oldLocale: "en_GB",
	oldDifficulty: "",
	oldsoundEnabled: true,
	difficulty: "easy",
	locales: ["en_GB", "es_ES"],
	weightStrings: ["lbs", "kgs"],	// points to the languagestrings 
	currencyStrings: ["dontshow", "sinewave","playnote"],	// points to the languagestrings 	
	offOnStrings: ["off", "on"],	// points to the languagestrings 	
	locale: "en_GB",
	soundEnabled: true,
	showMusic: "sinewave",
	showPrompt: true,
	weight: "kgs",
	
	initialise: function() {
		this.locale = LanguageStrings.CurrentLocale;
		this.oldLocale = this.locale;
		this.store();
		//this.apply();
	},
	
	CreateHTML: function() {
		// Note: Some parameters for the fields are set in the supporting css file
		var languageCombo = document.createElement("select");
		languageCombo.setAttribute("name", "options_language");
		languageCombo.setAttribute("id", "options_language");
		document.getElementById('maindiv').appendChild(languageCombo);
		languageCombo.onchange = function() {
			options.apply();
			MusicalBottles.doRender(); // change the text on the page
		}
		
		var languageOption;
		languageOption = document.createElement("option");
		languageOption.setAttribute("value", "en_GB");
		languageOption.innerHTML = "English";
		languageCombo.appendChild(languageOption);
		
		languageOption = document.createElement("option");
		languageOption.setAttribute("value", "es_ES");		
		languageOption.innerHTML = "Español";
		languageCombo.appendChild(languageOption);
		
		// Note: in IE, the font of the options in the dropdown will change to Times New Roman. Not sure this can be sorted

		
		// Add the combo box for units of weight (Imperial or Metric)
		var unitCombo = document.createElement("select");
		unitCombo.setAttribute("name", "options_weight");
		unitCombo.setAttribute("id", "options_weight");
		
		var unitOption;
		var unitCombo = document.createElement("select");
		unitCombo.setAttribute("name", "options_currency");
		unitCombo.setAttribute("id", "options_currency");
		
		// Add the combo box for currency (Dollars, Pounds or Euros)

		document.getElementById('maindiv').appendChild(unitCombo);
		unitCombo.onchange = function() {
			options.apply();
			MusicalBottles.doRender(); // change the text on the page
		}
		
		var unitOption;
		unitOption = document.createElement("option"); unitOption.setAttribute("value", this.currencyStrings[0]); unitOption.innerHTML = "Dollars";
		unitCombo.appendChild(unitOption);		
		unitOption = document.createElement("option"); unitOption.setAttribute("value", this.currencyStrings[1]); unitOption.innerHTML = "Pounds";
		unitCombo.appendChild(unitOption);
		unitOption = document.createElement("option"); unitOption.setAttribute("value", this.currencyStrings[2]); unitOption.innerHTML = "Euros";
		unitCombo.appendChild(unitOption);

		var checkbox = document.createElement('input');
		checkbox.type = "checkbox";
		checkbox.name = "namye";
		checkbox.value = "value";
		checkbox.id = "option_prompt_tickbox";
		document.getElementById('maindiv').appendChild(checkbox);
		checkbox.onchange = function() {
			options.apply();
			MusicalBottles.doRender(); // change the text on the page
		}
		
		var ctx = Mag2DScreen; //MusicalBottles.getCanvasContext();
		var size = Mag2DScreen.screenY(28);
	
		document.getElementById('options_language').style.left = Mag2DScreen.screenX(390) + "px"; ;		
		document.getElementById('options_language').style.top = Mag2DScreen.screenY(320) + "px"; 	
		document.getElementById('options_language').style.height = Mag2DScreen.screenY(45) + "px";
		
		document.getElementById('options_currency').style.height = Mag2DScreen.screenY(40) + "px"; 
		document.getElementById('options_currency').style.fontSize = size + "px";
		document.getElementById('options_currency').style.left = Mag2DScreen.screenX(390) + "px";
		document.getElementById('options_currency').style.top = Mag2DScreen.screenY(402) + "px";	
		 
		document.getElementById('option_prompt_tickbox').style.height = Mag2DScreen.screenY(40) + "px"; 
		document.getElementById('option_prompt_tickbox').style.fontSize = size + "px";
		// x/y-position varies with screensize - is it doing some centering within the cell?
		document.getElementById('option_prompt_tickbox').style.left = Mag2DScreen.screenX(390) + "px";
		document.getElementById('option_prompt_tickbox').style.top = Mag2DScreen.screenY(470) + "px";		
		},
	
	updateOptionsScreen: function() {
		// initialises the combo boxes etc when page started //refreshes the screen after apply() has been called
		console.log("update options screen."+options.locale+" "+options.weight+" "+options.currency+" "+options.showPrompt);
		var difficultySelector = document.getElementsByName('options_difficulty');
		var soundDiv = document.getElementById('options_sound');
		var language = document.getElementById('options_language');
		var showMusic = document.getElementById('options_currency');
		//var showPrompt = document.getElementById('options_prompt');
		var promptTickBox = document.getElementById('option_prompt_tickbox');
		console.log("selected tickbox is: " +promptTickBox.checked); 
		
		options.locale = LanguageStrings.getLocale();
		
		for (var i=0; i<language.options.length;i++) {
			language.options[i].text = LanguageStrings.getString(options.locales[i].split('_')[0]);
				if ( language.options[i].value === options.locale) {
				language.selectedIndex = i;
				//console.log("selected locale index is: " + i+ " "+options.locale); 
			}
		}			
		
		for (var i=0; i<showMusic.options.length;i++) {
			showMusic.options[i].text = LanguageStrings.getString(options.currencyStrings[i]);
			console.log("selected showMusic index is: " + i+ " "+showMusic.options[i].value+" "+options.showMusic); 
			if ( showMusic.options[i].value === options.showMusic) {
				showMusic.selectedIndex = i; 
				//console.log("selected showmusic index is: " + i+ " "+options.showMusic); 
			}
		}
		//console.log("options:currency is "+options.showMusic);
		
		/*for (var i=0; i<showPrompt.options.length;i++) {
			showPrompt.options[i].text = LanguageStrings.getString(options.offOnStrings[i]);
			//console.log("selected showPrompt index is: " + i+ " "+showPrompt.options[i].value+" "+options.showPrompt); 
			if ( showPrompt.options[i].value === options.showPrompt) {
				showPrompt.selectedIndex = i; 
				//console.log("selected showPrompt index is: " + i+ " "+options.showPrompt); 
			}
		}*/
		// console.log("selected tickbox is: " +promptTickBox.checked+" "+options.showPrompt);
	    promptTickBox.checked = options.showPrompt;

/* no difficulty level
		for (var i=0;i<difficultySelector.length;i++) {
			document.getElementById(difficultySelector[i].value+"_label").innerHTML=LanguageStrings.getString(difficultySelector[i].value);
			
			if(difficultySelector[i].value == options.difficulty) {
				difficultySelector[i].checked = true;
			} else {
				difficultySelector[i].checked = false;
			}
		}*/
	},
	
	RemoveHTML: function() {
		document.getElementById('options_sound').innerHTML = ''	;	
		document.getElementById('options_difficulty_container').innerHTML = '';
		
		// Note of Caution!! The following may not work for all broswers (Chrome & IE ok)
/*		document.getElementById('options_language').style.top = "800px";  // desperation to get rid of language option when exitting
		document.getElementById('options_weight').style.top = "800px";  
		document.getElementById('options_currency').style.top = "800px";  
		document.getElementById('options_difficulty_container').style.zIndex = 0;
		document.getElementById('options_language').style.zIndex = 0;
		document.getElementById('options_weight').style.zIndex = 0;  
		document.getElementById('options_currency').style.zIndex = 0;	*/	
		var element = document.getElementById("options_language"); element.parentNode.removeChild(element);	
		//element = document.getElementById("options_weight"); element.parentNode.removeChild(element);			
		element = document.getElementById("options_currency"); element.parentNode.removeChild(element);
		//element = document.getElementById("options_prompt"); element.parentNode.removeChild(element);
		element = document.getElementById("option_prompt_tickbox"); element.parentNode.removeChild(element);
		//document.getElementById('options_language').innerHTML = '';		
	},
	
	store: function() {
		this.oldLocale = this.locale;
		this.oldDifficulty = this.difficulty;
		this.oldsoundEnabled = this.soundEnabled;
		this.oldShowMusic = this.showMusic;
		this.oldShowPrompt = this.showPrompt;		
		this.oldWeight = this.weight;		
	},
	
	apply: function() {
		// callback, called when an option (ie from a combo box) changed
		
		var difficultySelector = document.getElementsByName('options_difficulty');
		var clockSelector = document.getElementsByName('options_clock');
		var language = document.getElementById('options_language');
		var showMusic = document.getElementById('options_currency');
		// prompt pull down 
		var showPrompt = document.getElementById('options_prompt');
		var promptTickBox = document.getElementById('option_prompt_tickbox');
		console.log("selected tickbox is: " +promptTickBox.checked); 		
		
		this.locale = language.options[language.selectedIndex].value;
		this.showMusic = showMusic.options[showMusic.selectedIndex].value;
		// prompt pull down this.showPrompt = showPrompt.options[showPrompt.selectedIndex].value;
		this.showPrompt = promptTickBox.checked;
		LanguageStrings.setLocale(this.locale);		
		
		for (var i=0;i<difficultySelector.length;i++) {
			if(difficultySelector[i].checked == true) {
				options.difficulty = difficultySelector[i].value;
			}
		}
	
		console.log("applying options");
		console.log("Locale: " + this.locale);
		//console.log("Sound: " + this.soundEnabled);
		//console.log("Difficulty: " + this.difficulty);
		//console.log("Weight: " + this.weight);		
		
		//LanguageStrings.setLocale(this.locale);
		//Application.soundEnabled = this.soundEnabled;
		options.updateOptionsScreen();		
	},
	
	revert: function() { // not used by materials i believe
	    this.unused = -1; // not used by materials i believe
		this.locale = this.oldLocale;
		this.difficulty = this.oldDifficulty;
		this.soundEnabled = this.oldsoundEnabled;
		this.apply();
	}
};

