﻿ClickZones = {
       homeinstructions_en_GB: { scene: "home", left: 100, top: 970, right: 560, bottom: 1050, active: true, active: true, callback: function () { Dressing.showInstructions(); } },
       homesettings_en_GB: { scene: "home", left: 660, top: 970, right: 660 + 328, bottom: 1050, active: true, active: true, callback: function () { Dressing.showSettings(); } },
       homereports_en_GB: { scene: "home", left: 1107, top: 970, right: 1107 + 310, bottom: 1050, active: true, callback: function () { Dressing.showReports(); } },
       homeplay_en_GB: { scene: "home", left: 1608, top: 970, right: 1608 + 185, bottom: 1050, active: true, callback: function () { Dressing.showGame(); } },

       homeinstructions_es_ES: { scene: "home", left: 100, top: 970, right: 540, bottom: 1050, active: true, active: true, callback: function () { Dressing.showInstructions(); } },
       homesettings_es_ES: { scene: "home", left: 600, top: 970, right: 660 + 470, bottom: 1050, active: true, active: true, callback: function () { Dressing.showSettings(); } },
       homereports_es_ES: { scene: "home", left: 1142, top: 970, right: 1142 + 372, bottom: 1050, active: true, callback: function () { Dressing.showReports(); } },
       homeplay_es_ES: { scene: "home", left: 1608, top: 970, right: 1608 + 195, bottom: 1050, active: true, callback: function () { Dressing.showGame(); } },

       homefullscreen: { scene: "home", left: 1830, top: 40, right: 1900, bottom: 110, active: true, callback: function () { Dressing.fullscreenToggle(); } },

       leaveinstructions: { scene: "instructions", left: 905, top: 978, right: 905 + 80, bottom: 978 + 80, active: true, callback: function () { Dressing.hideInstructions(); } },

       leavereports: { scene: "reports", left: 920, top: 978, right: 920 + 80, bottom: 978 + 80, active: true, callback: function () { Dressing.hideReports(); } },

       scrollup: { scene: "game", left: 1504, top: 108, right: 1504+70, bottom: 108+70, active: true, callback: function () { Dressing.scrollUp(); } },
       scrolldown: { scene: "game", left: 1504, top: 814, right: 1504 + 70, bottom: 814 + 70, active: true, callback: function () { Dressing.scrollDown(); } },

       leavegame: { scene: "game", left: 900, top: 990, right: 900 + 120, bottom: 990 + 90, active: true, callback: function () { Dressing.leaveGame(); } },
       thermometer: { scene: "game", left: 1260, top: 30, right: 1300 + 64, bottom: 30 + 540, active: false, callback: function () { Dressing.thermometerClick(); } },

       settingsclose: { scene: "settings", left: 904, top: 770, right: 904 + 58, bottom: 770 + 60, active: true, callback: function () { Dressing.closeSettings(); } },

       settingssound: { scene: "settings", left: 0, top: 0, right: 0, bottom: 0, active: true, callback: function () { Dressing.Options.sound = !Dressing.Options.sound; Dressing.render(); } },
       settingschecking: { scene: "settings", left: 0, top: 0, right: 0, bottom: 0, active: true, callback: function () { Dressing.Options.checking = !Dressing.Options.checking; Dressing.render(); } },
       settingscelsius: { scene: "settings", left: 0, top: 0, right: 0, bottom: 0, active: true, callback: function () { Dressing.Options.temperature = "C"; Dressing.render(); } },
       settingsfarenheit: { scene: "settings", left: 0, top: 0, right: 0, bottom: 0, active: true, callback: function () { Dressing.Options.temperature = "F"; Dressing.render(); } },
       settingsen: { scene: "settings", left: 0, top: 0, right: 0, bottom: 0, active: true, callback: function () { Dressing.Options.language = "en_GB"; Dressing.render(); } },
       settingses: { scene: "settings", left: 0, top: 0, right: 0, bottom: 0, active: true, callback: function () { Dressing.Options.language = "es_ES"; Dressing.render(); } },
};

