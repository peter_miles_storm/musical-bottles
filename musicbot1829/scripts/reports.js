function ReportLine() {
	var Title;
	var Correct;
	var Incorrect;
	var Total;
};

ReportLine.prototype.constructor = function(title) {
	this.Title = title;
	this.Correct = 0;
	this.Incorrect = 0;
	this.Total = 0;
};

ReportLine.prototype.increment = function(prop) {
	this[prop]++;
	this.Total++;
};

ReportLine.prototype.incValue = function(prop, value) {
// increments the value stored by the value passed in
	this[prop] += value;
	this.Total += value;
};

function Reports() {
	var ReportLines;
	var RowHeight;
	var ColumnWidth;
	var RowTitleWidth;
	
	var RowColours, HeaderColour;
	
	var SigmaImage, TickImage, CrossImage;
};

// arguments is an array of titles for each report line
Reports.prototype.constructor = function(titles) {
	this.ReportLines = [];
	this.RowHeight = 60;
	this.ColumnWidth = 100;
	this.RowTitleWidth = 400;
	
	this.RowColours = ["rgb(246, 211, 145)", "rgb(250, 230, 195)"];
	this.HeaderColour = "rgb(270, 250, 215)";
	
	var r = null;
	
	for (var i = 0; i < titles.length; i++) {
		r = new ReportLine();
		r.constructor(titles[i]);
		this.ReportLines.push(r);
	}
	
	this.TickImage = new Image();
	this.TickImage.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAASCAYAAACAa1QyAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAEJSURBVCiRjdK9SgNBFMXx30YJKNhYpLUR7ETU1sLCxtpGBF9AfAI70cLKztbHSOFb2GtARI2gIG78JI7NXViTrNmBZdl79z/n3DMjpaTOg0Pc4jSLwtiVZVkXLTw1agKrmI7Pfi0I22gioV13nocAXrBVB1jBa0BfmG2E5/ksy6bGWIPrlNIzbKCH4wql+1Dp4aBI+yaK75gbAJZL1j7RSilpoEhwEmcV1n7QTik9Fo1d5LHbG9YrrK2V6ibQQT9+uIraUsla54/tIBdKajn2cRJz5NgbggLcjDBSKHRLAc2MhAI8Cv8f8f7G+dAxDEBN3IVCEcziv1DJZjHf5cgDr7gFFwHtjOr/AtThUOXjji3dAAAAAElFTkSuQmCC";
	this.CrossImage = new Image();
	this.CrossImage.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAOCAYAAAD0f5bSAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAADtSURBVCiRddKtSoRBFMbxHy8iGgyuWTCLYBKTYBU0eQVGm7hV2Dsw7B3IegUWo0abSVCwGESbYFj8gB3L2WX27OvAhPOc/zPzzAf0UGL2SinyxFnFnECD6xB+sZUM6/iO/lUpxbjRwUs0HrEYeoO70N+wMjEFsItRAP3QTqtYBxM2RTkPYIRjDKMeTHHJtICHavWCd3T+NYVxO5kOM9OYHXupnp8h0i6b+Kmuv+ADq63xMIf7AF+xg6+ob9C0meqfsR9at9K6U6Y4/DjORbVQg9vQh9gI3RKeq1jL6Zxr+Iz+U/Auc6yWZziqmMEf2RsGTdJctMYAAAAASUVORK5CYII=";
	this.SigmaImage = new Image();
	this.SigmaImage.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAQCAYAAADAvYV+AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAC2SURBVCiRjdIxTkJBEIfx3yYPKgoLISZKZ+EFuAYHgMaY2HgFj8IF7EiInoCSyhJrYwE0GBIpSB7FGyvg7Ztktvq+f3Z2B6YoG/RjgR9V7bF0Wm1cheAaaxwwKMvSpa4ORmF+olULh/ARwmsTuI/fuPtDLRzCS6TPkXJwwiKEpxx8hy12uM/B75H6nBtwHOAs985dbLBCLwe/Reqw9gcxDHCSAVPCN27xhb8zi1Sgg5v0v01N6ggP9TIWuvrDyQAAAABJRU5ErkJggg==";
};

// which is either Correct or Incorrect
Reports.prototype.increment = function(title, which) {
	var index = 0;
	while (index<this.ReportLines.length) {
		//console.log("increment "+this.ReportLines[index].Title+" "+title);
		if (this.ReportLines[index].Title===title) break;
		index++;
	}
	
	this.ReportLines[index].increment(which);
};


Reports.prototype.incValue = function(title, which, value) {
// increments the value stored by the value passed in
	var index = 0;
	while (index<this.ReportLines.length) {
		if (this.ReportLines[index].Title===title) break;
		index++;
	}
	
	this.ReportLines[index].incValue(which, value);
};

Reports.prototype.Reset = function() {
	this.ReportLines.forEach(function(element, index, array) { 
			element.Correct = 0;
			element.Incorrect = 0;
			element.Total = 0;
	 });
};

Reports.prototype.getAllData = function() {
// used when data stored to local browser storage
	return this.ReportLines;
};

Reports.prototype.setAllData = function(allData) {
// when data loaded from local browser storage. It only works when you load data field by field

	//console.log("setalldata "+index);
	for(var index=0;index<this.ReportLines.length;index++) {
			//console.log("increment "+allData[index].Correct); //+this.ReportLines[index].Title+" "+title);
			this.ReportLines[index].Correct = allData[index].Correct;
			this.ReportLines[index].Incorrect = allData[index].Incorrect;
			this.ReportLines[index].Total = allData[index].Total;
	}	

};
;

Reports.prototype.render = function(Mag2DScreen, canvaswidth, canvasheight) {
	var left = 150,
		top = 300,
		width = this.RowTitleWidth+this.ColumnWidth*3,
		x, y,
		colourindex = 0,
		s, tm;
		
		if (MusicalBottles.adMob) top -= 60;
	y = top + (-1*this.RowHeight);

	Mag2DScreen.drawImage(MusicalBottles.homeControls[2], (512 - 124), top-208);
	Mag2DScreen.font = "32px andika_basicregular";
	Mag2DScreen.fillStyle = "white";
	Mag2DScreen.setFillStyle("white");
	s = LanguageStrings.getString("reports");
	tm = Mag2DScreen.measureText(s);	
	//Mag2DScreen.fillText(s, 512 - (Mag2DScreen.textWidth(s)/*tm.width*//2), 679);
	Mag2DScreen.fillText(s, ((canvaswidth - Mag2DScreen.textWidth(s)/*tm.width*/)/2), top-160);
	// reset icon
	Mag2DScreen.fillStyle = "white";	
	s = LanguageStrings.getString("resetreports");
	tm = Mag2DScreen.measureText(s);	
	if (!MusicalBottles.adMob) {
		Mag2DScreen.drawImage(MusicalBottles.homeControls[3], (512 - 124), top + 332);
			Mag2DScreen.fillText(s, 512 - 12 - (Mag2DScreen.textWidth(s)/*tm.width*//2), 679);
	} else {
		Mag2DScreen.drawImage(MusicalBottles.homeControls[3], (512 - 124), top + 272);
			Mag2DScreen.fillText(s, 512 - 12 - (Mag2DScreen.textWidth(s)/*tm.width*//2), 559);		
	}

	//Mag2DScreen.fillText(s, 512 - (tm.width/2), 679);
	
	Mag2DScreen.font = "24px andika_basicregular";
	for (var i = 0; i < 4 /*this.ReportLines.length*/; i++) {
		y = top + (i*this.RowHeight);
		
		Mag2DScreen.fillStyle = this.RowColours[colourindex];
		Mag2DScreen.setFillStyle(this.RowColours[colourindex]);
		Mag2DScreen.fillRect(left, y, width, this.RowHeight);
		
		Mag2DScreen.fillStyle = "black";
		Mag2DScreen.setFillStyle("black");		
		Mag2DScreen.fillText(LanguageStrings.getString(this.ReportLines[i].Title), left+10, y+this.RowHeight-9);
		
		s = ""+this.ReportLines[i].Total;
		if (this.ReportLines[i].Title == 'duration') {
		   var mm = Math.floor(this.ReportLines[i].Total/60);
		   s = "" + mm + ":";
		   var ss = Math.floor(this.ReportLines[i].Total - (60*mm));
		   if (ss < 10)
		     s = s + "0";
		   s = s + ss;
		   //console.log("times "+this.ReportLines[i].Total+" "+mm+":"+ss+" "+s);
		}
		tm = Mag2DScreen.measureText(s);
		Mag2DScreen.fillText(s, left+this.RowTitleWidth + ((this.ColumnWidth - Mag2DScreen.textWidth(s)/*tm.width*/)/2)+this.ColumnWidth*2, y+this.RowHeight-9);

		colourindex = colourindex ^ 1;
		y+=this.RowHeight;
	}
	
	y = top + (-1*this.RowHeight);
	Mag2DScreen.fillStyle = this.HeaderColour;
	Mag2DScreen.fillRect(left, y, width, this.RowHeight);
	
	// grey right coloumn containing numbers
	Mag2DScreen.fillStyle = "rgba(0, 0, 0, 0.1)";	
	Mag2DScreen.fillRect(left + this.RowTitleWidth + (this.ColumnWidth*2), top - this.RowHeight, this.ColumnWidth, (this.ReportLines.length+1)*this.RowHeight);
	//x = left + this.RowTitleWidth + ((this.ColumnWidth - this.SigmaImage.width)/2) + this.ColumnWidth*2;
	//y = top - 8 - this.SigmaImage.height;
	//Mag2DScreen.drawImage(this.SigmaImage, x, y);
	
		Mag2DScreen.beginPath();
		Mag2DScreen.lineWidth=3;		
		for(var i=-1;i<5;i++) {
			y = top + (i*this.RowHeight);
			var add = 0;
			if (i == 4) add = 1;
			Mag2DScreen.moveTo(left-add, y);
			Mag2DScreen.lineTo(left + this.RowTitleWidth + (this.ColumnWidth*3) + add, y);
		}
		// vertical lines. The -1 are to make the corners complete
		Mag2DScreen.moveTo(left, top - this.RowHeight-2); Mag2DScreen.lineTo(left, top + (4*this.RowHeight)+1);
		Mag2DScreen.moveTo(left + this.RowTitleWidth + (this.ColumnWidth*2), top - this.RowHeight); Mag2DScreen.lineTo(left + this.RowTitleWidth + (this.ColumnWidth*2), top + (4*this.RowHeight));
		Mag2DScreen.moveTo(left + this.RowTitleWidth + (this.ColumnWidth*3), top - this.RowHeight-2); Mag2DScreen.lineTo(left + this.RowTitleWidth + (this.ColumnWidth*3), top + (4*this.RowHeight));
		Mag2DScreen.stroke();
};
