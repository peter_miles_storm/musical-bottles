
//
// --- HTML/Dom/Browser functions) ---
//
	
document.addEventListener('DOMContentLoaded', function () {
  			
  // After page content is ready (onBodyLoad)
  // TODO: check screen width and height and set MusicalBottles.CanvasWidth,Height
	if (MusicalBottles.Reporting===null) {
		MusicalBottles.Reporting = new Reports();
		var titles = [];
		for (var index = 0; index<cfg.topics[0].reports.length/*cfg.topics.length*/; index++) {
			name = cfg.topics[0].reports[index].name;
			console.log("rep names "+name);
			titles.push(name /*cfg.topics[index].title*/);
		}
		MusicalBottles.Reporting.constructor(titles);
	}
  LanguageStrings.setLocale();	
  MusicalBottles.createGameCanvas();
	
  //console.log("bef call to resize");
  window.onresize();
  //Application.onresize();
  MusicalBottles.loadLanguageSetting(); // requires that this.Reporting has been created
  document.title=LanguageStrings.getString("title");
  MusicalBottles.playAudio("bottles"+MusicalBottles.soundType);  
});

// Strict is good
"use strict";


window.requestAnimFrame = (function(time){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000/60);
          };
})();

(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener( 'pause', onPause.bind( this ), false );
        document.addEventListener( 'resume', onResume.bind( this ), false );
        
        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };

    /* this is done in the nor mal code
	document.addEventListener('DOMContentLoaded', function () {
        MusicalBottles.createGameCanvas();
    });*/

    window.onresize = function () {
        Mag2DScreen.resize(MusicalBottles.render);
    };
})();

  // window changing size / rotation
  // this overrides the window function and is considered poor coding!!

   window.onresize = function() {
        	var d = document.getElementById("maindiv");
        	var w = 1024;
			
			MusicalBottles.MainMaxX = w;
        	//var h = 768;
			var h = 658; // no longer sure what the advantage of 658 is ...
			if (MusicalBottles.isIpad)
			   h = 768;
        
   			/*var leftx = ""+ ((window.innerWidth - w) / 2) + "px";
			var topy = 0;
			if (window.innerHeight > h) {
				topy = ""+ ((window.innerHeight - h) / 2) + "px";
			} else {
				topy ="0px";
			}
			//console.log("on resize "+leftx);
			MusicalBottles.LeftX = ((window.innerWidth - w) / 2);
			MusicalBottles.TopY = ((window.innerHeight - h) / 2);
   			d.style.left = leftx;
   			d.style.top = topy;*/
	};
			


//
// --- Main Class for Musical Bottles App ---
//
		
MusicalBottles = {
// new mag2d	
    GameCanvasCreated: false,

    CurrentScene: "",
    RenderScene: null,
	
	
   MainMaxX: 0,
   LeftX: 0,
   TopY: 0,
   hgt: 768,
   iPadHeightAdjust: 20,
   adMob: false,
   adMobHeightAdjust: 0, // set up later on 
   CanvasWidth: 1024,
   // CanvasHeight: 768,
   CanvasHeight: 748, // on ipad the working height is reduced by the apple header (20 pixels), 
   GameCanvasCreated: false,
   ObjectInitialTop: 90,
   
 	REP_RECORDED: 1,		// offsets for reporting
	REP_PLAYED: 2,
	REP_NOTES: 0,
	REP_DURATION: 3,		// (in seconds)
	
   Locations: ["loading","home","instructions","settings","reports","game"],
   Location: "loading",
   render: false,
   
   CurrentNoteIndex: 0,
   PlaybackIndex: 0,
   MusicalNotes: new Array,
   Bottles: new Array, 
   bottleTop: 234, 
   MaxBottles: 5,
   Jug: null,
   drumstick: null,
   xdrumstick : -1, // if drumsticks need displaying
   ydrumstick : -1,  
   Smudge: null,
   setupInitialised: false,
   ActivityTime: 0,
   ActivityStart: 0, // time the activity started
   
   Playback: false,
   Recording: false,   
   RecordingTime: 0,
   RecordingItems: null,
   RecordingItem: 
				{
					Bottle: null,
					Time: null,
					Callback: null,
				},
    emptying: false,
	bottleFilling: null,		// the bottle being filled (so we can stop on exit from activity)
	
	Buttons: new Array,
	CurrentTopic: -1,
	numItems: 7, // 7 normally, 2 for demoss
	backgrounds: [],
	
	buttonUp: [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],  
	buttonDown: [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],  
	buttonPressedTime: [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],
	animScientist:[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],
	animFlame:[null,null,null],
	showNotes:[null,null,null,null],
	homeControls:[null,null,null,null,null],
	homeTime:[0,0,0,0,0],
	recycle:[null,null,null,null],
	logoImage : null,
	backButton: null, 
	backDownButton: null, 
	blinkTime: 0,					// time used for animating the mad scientist/
	xwlscale: 12.0, // number of degrees per pixel
	flameTime: 0,
	recycleTime: 0,
	rc: 0, 							// frame number for recycle 
	nFlame: 0,						// flame frame to show
	yBack: 684, // y-position of back icon for insructions and reporting
	icnAudio:   {x: 947, y: 690, w: 65, h: 44},
	soundPlaying: 0,
	helpAudioPlaying: false,
	backTime: 0, // non-zero when back icon pressed
	resetTime: 0, // non-zero when reset icon pressed
	// attributes for 
	showSinewave: false,
	outputs: null,
	wLength: [null, null, null, null, null, null, null, null, null, null],	
	pitch: [null, null, null, null, null, null, null, null, null, null],  // no. of points in sine wave
	sx: [null, null, null, null, null, null, null, null, null, null],	  // start position of sine wave (0=lhs, 204 = rhs)
	MAXHITS: 20, 					  // max number of sine waves that can be shown
		sheetmusicRect: {
		left: 640,
		top: 80 //154
	},
	noteOffset: 20,
	ix: 600,
	oneshotPourSound: true,
	

	//
	// --- Initialising and Setup Activity ---
	//
		
	createGameCanvas: function () {
		
        Mag2DScreen.setOriginalSize({ width: 1024, height: 768 });
        Mag2DScreen.calculateScreenDimension(0); // was 110 for adsense
        Mag2DScreen.createScreenCanvas("maindiv");

        var canvas = Mag2DScreen.ScreenCanvas;
        // 'click' is fired along with mousedown when left button clicked 
		//canvas.addEventListener('click', function (event) { MusicalBottles.canvasClicked(event); }, false);
        addWheelListener(canvas, function (e) { Dressing.scrollWheel(e.deltaY); e.preventDefault(); });
		
       /* create the canvas for the main gamescreen
       var canvas = document.createElement("canvas");
       canvas.id = "gamecanvas";
       canvas.width = MusicalBottles.CanvasWidth;
       canvas.height = MusicalBottles.CanvasHeight;
       canvas.style.position = "absolute";
		
       //canvas.addEventListener('click', function(event) { MusicalBottles.canvasClicked(event); }, false);

       document.getElementById("maindiv").appendChild(canvas);*/
		
       canvas.addEventListener('mousedown', MusicalBottles.ev_mousedown, false);
       canvas.addEventListener('mousemove', MusicalBottles.ev_mousemove, false);
       canvas.addEventListener('mouseup', MusicalBottles.ev_mouseup, false);
	   canvas.addEventListener('mouseover', MusicalBottles.ev_mouseover, false);
		
       MusicalBottles.GameCanvasCreated = true;

	   document.title=LanguageStrings.getString("title");
	   MusicalBottles.setSoundFormat();	 
	   //this.playAudio("bottles");
	   
	   options.initialise();
       // load all bitmaps
       MusicalBottles.loadBitmaps(true);

	   
	   if (MusicalBottles.demo) {
	     MusicalBottles.numItems = 2;
	   } else {
	     MusicalBottles.numItems = 7;
	   }
		var d = document.getElementById("maindiv"); d.setAttribute("height", "658px");	 // this works, but does not cause image to be 658 for use in a browser.

	   if (MusicalBottles.adMob) {
	     MusicalBottles.adMobHeightAdjust = 90;
		 MusicalBottles.ObjectInitialTop = 50;
		 MusicalBottles.sheetmusicRect.top	-= 70;	 
		 MusicalBottles.yBack-= 40;
		 cfg.topics[0].buttons[3].rect.top -= 40;
		 this.icnAudio.y -= MusicalBottles.adMobHeightAdjust;	
		 // the following does nothing - included in case a solution is found
		 MusicalBottles.hgt = "658";	
		 var d = document.getElementById("maindiv"); d.setAttribute("height", "658px");	 // this works, but does not cause image to be 658 for use in a browser.
		 window.onresize();
		 } else {
	     MusicalBottles.adMobHeightAdjust = 0;	 
		 MusicalBottles.ObjectInitialTop = 90;
		MusicalBottles.hgt = "658"; //"768";		 
	   }
	   //console.log("canvas created "+MusicalBottles.yBack);	   
   },
	
	Bitmaps: ["bg"],
	GameImagesLoader: null,
	GameImages: {
			bg_en_GB:	{
					src: "images/en_GB/menu768.png" //"musicalbottlesbg768.png" 
				},
			bg_es_ES:	{
					src: "images/es_ES/menu768.png" // "musicalbottlesbg768.png" 
				},
			instructions:	{
					src: "images/bgplain768.png"  
				},
			ipanel:	{
					src: "images/instruction_panel768.png"  
				},						
			audioOff: {
				src: "images/soundoff.png" //"audioOff.gif"
			},
			audioOn: {
				src: "images/sound1.png" //"audio.gif"
			},
			audioPlay: {
				src: "images/sound2.png" //"audio.gif"
			},			
			sheetmusic: {
				src: "images/bg_music.png"
			},
			sinewave: {
				src: "images/bg_sinewave3.png"
			},
			speech: {
				src: "images/speech_bubble.png"
			},
			note: {
				src: "images/notes2.png"
			}
			
				
	},
	loadBitmaps: function (startup) {
        MusicalBottles.render = false;
		var s;
		var hgt = MusicalBottles.hgt; // as a string (if needed)
		var imagearray = new Array();
		console.log("height "+MusicalBottles.hgt+" "+this.hgt);
		for (var prop in MusicalBottles.GameImages) {
			if (MusicalBottles.GameImages[prop].image!==undefined) {
				delete MusicalBottles.GameImages[prop].image;
			}

			if (MusicalBottles.GameImages[prop].src.indexOf(options.oldLocale)) {
				s = MusicalBottles.GameImages[prop].src.replace(options.oldLocale, LanguageStrings.CurrentLocale);
			} else {
				s = MusicalBottles.GameImages[prop].src;
			}
			if (MusicalBottles.adMob /*(MusicalBottles.hgt !="768")*/ && (s.indexOf("768") > -1)) {
				s = s.replace("768", "658");
			}		
			console.log("loadbitmap "+s);
			imagearray.push(s);
		}
		
		for (var t=0; t<cfg.getNumberTopics(); t++) {
            //imagearray.push("images/"+cfg.topics[t].file);
            s = "images/"+cfg.topics[t].file;
				if (MusicalBottles.adMob && (s.indexOf("768") > -1)) {
					s = s.replace("768", "658");
			}
			imagearray.push(s);	
            //console.log("loadbitmap "+ "images/"+cfg.topics[t].file);
            /* no longer used s = "images/"+cfg.topics[t].fgfile;
				if (MusicalBottles.adMob && (s.indexOf("768") > -1)) {
					s = s.replace("768", "658");
			}
			imagearray.push(s);	*/	
            //console.log("loadbitmap "+ "images/"+cfg.topics[t].file);
			for (var i=0; i<cfg.topics[t].items.length; i++) {
				imagearray.push("images/"+cfg.topics[t].items[i].file);
				//console.log("loadbitmap "+ "images/"+cfg.topics[t].items[i].file);
                var animation = cfg.topics[t].items[i].animation;
                if (typeof animation !== "undefined") {
                    for (var img in animation) {
                        imagearray.push("images/"+ animation[img]);
				        //console.log("loadbitmap "+ "images/" +  animation[img]);
                    }
                }
			}
            
            for (var button in cfg.topics[t].buttons) {
                imagearray.push("images/"+cfg.topics[t].buttons[button].down_img);
                //console.log("loadbitmap "+ "images/"+cfg.topics[t].buttons[button].grey_img);
                imagearray.push("images/"+cfg.topics[t].buttons[button].up_img);
                imagearray.push("images/"+cfg.topics[t].buttons[button].grey_img);
            }
		}
		//console.log("outside loop");
		MusicalBottles.GameImagesLoader = new ImagePreloader(imagearray, MusicalBottles.bitmapsLoaded);
		
		//for(var i = 0; i < this.backFiles.length; i++)
		//	this.backgrounds.push(MusicalBottles.GetImage("images/" + this.backFiles[i] + this.hgt+".png"));
		
		for(var i = 0; i < 7; i++)
		{
		    t = 0; // bodge
			this.buttonUp[i] = MusicalBottles.GetImage("images/" + cfg.topics[t].buttons[button].up_img);
			this.buttonDown[i] = MusicalBottles.GetImage("images/" +cfg.topics[t].buttons[button].down_img);
		}
		
		this.logoImage = MusicalBottles.GetImage("images/logo.png");
		this.backButton = this.GetImage("images/backdark1.png"); 
		this.backDownButton = this.GetImage("images/backdark2.png"); 		
		this.drumstick = MusicalBottles.GetImage("images/drumstick.png");
		for (var i=0;i<1;i++) { 
		   this.animScientist[2*i] = this.GetImage("images/scientist/" + (1+i) + ".png");
		   this.animScientist[2*i+1] = this.GetImage("images/scientist/" + (1+i) + "blink.png");
		}	
		for (var i=0;i<3;i++) { 
		   this.animFlame[i] = this.GetImage("images/flame" + (1+i) + ".png");
        }
		for (var i=0;i<4;i++) { 
		   this.showNotes[i] = this.GetImage("images/notes" + (5-i) + ".png");
        }
        var controls = ["start", "instructions", "reports", "reset", "options"];
		for (var i=0;i<5;i++) { 
		   this.homeControls[i] = this.GetImage("images/" + controls[i] + "1.png");
        }
		for (var i=0;i<4;i++) { 
		   this.recycle[i] = this.GetImage("images/recycle" + (i*90) + ".png");
        }
		
	},
	
	bitmapsLoaded: function () {
		
		var j = 0;
		for (prop in MusicalBottles.GameImages) {
			MusicalBottles.GameImages[prop].image = MusicalBottles.GameImagesLoader.getImage(j);
			j++;
		}

		for (var t=0; t<cfg.getNumberTopics(); t++) {
            cfg.topics[t].image = MusicalBottles.GameImagesLoader.getImage(j);
            //console.log("*** - " +cfg.topics[t].image.src);
            j++;
            // no longer used cfg.topics[t].fg = MusicalBottles.GameImagesLoader.getImage(j);
            //console.log("*** - " +cfg.topics[t].fg.src);
            // no longer used j++;
			for (var i=0; i<cfg.topics[t].items.length; i++) {
				cfg.topics[t].items[i].image = MusicalBottles.GameImagesLoader.getImage(j);
				//console.log("*** - " +cfg.topics[t].items[i].image.src);
				j++;
                var animation = cfg.topics[t].items[i].animation;
                if (typeof animation !== "undefined") {
                    cfg.topics[t].items[i].anim = new Array;
                    for (var img in animation) {
                        cfg.topics[t].items[i].anim.push(MusicalBottles.GameImagesLoader.getImage(j));
				        //console.log("*** - " +cfg.topics[t].items[i].anim[img].src);
                        j++;
                    }
                }
			}
			
            for (var button in cfg.topics[t].buttons) {
                cfg.topics[t].buttons[button].down = MusicalBottles.GameImagesLoader.getImage(j);
                //console.log(t+" *** - " +cfg.topics[t].buttons[button].down.src);
                j++;
                 cfg.topics[t].buttons[button].up = MusicalBottles.GameImagesLoader.getImage(j);
				j++;				 
                //console.log("*** - " +cfg.topics[t].buttons[button].up.src);

                cfg.topics[t].buttons[button].grey = MusicalBottles.GameImagesLoader.getImage(j);
				//console.log("*** - " +cfg.topics[t].buttons[button].grey.src);				
				j++;
            }
		}
          
        // this device means doRender() is called continuously		  
		if (MusicalBottles.Location==='loading') {
            (function animloop(){
              requestAnimFrame(animloop);
                if (MusicalBottles.render) {
                    MusicalBottles.doRender();
                }
            })();
			MusicalBottles.Location = 'home';
		}
		MusicalBottles.render = true;
		/*MusicalBottles.Location = "home";		
		MusicalBottles.doRender();	
 	    console.log("on return from dorender");*/
	},


   GetImage: function (fileName) { 

		var newImg = new Image();
		newImg.src = fileName;
		return newImg;
	},
	
	
	//
	// --- DISPLAY (RENDER) GRAPHICS ON THE SCREEN ---
	//
	
	getCanvasContext: function () {
		var canvas = document.getElementById("gamecanvas");
        return canvas.getContext("2d");
	},
			

	
	doRender: function () {
	    //console.log("dorender "+MusicalBottles.Location);
		switch (MusicalBottles.Location) {
			case "loading":
				MusicalBottles.renderLoading();
				break;
				
			case "home":
				MusicalBottles.renderHome();
				break;
				
			case "instructions":
				MusicalBottles.renderInstructions();
				break;
				
			case "game":
				MusicalBottles.renderGame();
				break;
				
			case "reports":
				this.renderReports();
				break;
			case "options":
				this.renderOptions();
				break;
			default: 
				break;
		}
	},
	
	// Not used for Musical Bottles
	renderLoading: function () {
	},
    
	
	backButton: function () {
		if (this.backTime == 0)
			Mag2DScreen.drawImage(MusicalBottles.backButton, 16, MusicalBottles.yBack);
		else {
			if ((this.getTime() - this.backTime) > 300) {
				this.backTime = 0;
				Mag2DScreen.drawImage(MusicalBottles.backButton, 16, MusicalBottles.yBack);
				MusicalBottles.Location = "home";	
			} else
				Mag2DScreen.drawImage(MusicalBottles.backDownButton, 16, MusicalBottles.yBack);
		}
	},
		
	soundZone: { left: 390, width: 67, top: 240, height: 46},
	renderOptions: function () {
		var ctx = this.getCanvasContext();

		Mag2DScreen.drawImage(this.GameImages["instructions"]["image"], 0, 0);
		Mag2DScreen.drawImage(MusicalBottles.GameImages["ipanel"]["image"], 0, 0);
		
		var y = 80; var xb = 512; var wb = 248;
		Mag2DScreen.drawImage(MusicalBottles.homeControls[4], (xb - 124), y);	
		Mag2DScreen.fillStyle = "white";
		var s = LanguageStrings.getString("options");

		//var tm = Mag2DScreen.measureText(s);
		var x = (wb - Mag2DScreen.textWidth(s)/*tm.width*/)/2;
		Mag2DScreen.fillText(s, xb - 124 + x , y+48);	
			
		Mag2DScreen.font = "28px andika_basicregular";
		Mag2DScreen.fillStyle = "black";
		
		Mag2DScreen.fillText(LanguageStrings.getString("audio"), 120 , this.soundZone.top + 18);
		if (options.soundEnabled) {	
			Mag2DScreen.drawImage(this.GameImages["audioOn"]["image"], this.soundZone.left, this.soundZone.top - this.soundZone.height/2 + 9);
			Mag2DScreen.fillText(LanguageStrings.getString("on"), this.soundZone.left + 100, this.soundZone.top + 18);
		} else {
			Mag2DScreen.drawImage(this.GameImages["audioOff"]["image"], this.soundZone.left, this.soundZone.top - this.soundZone.height/2 + 9);
			Mag2DScreen.fillText(LanguageStrings.getString("off"), this.soundZone.left + 100, this.soundZone.top + 18);
		}

		Mag2DScreen.fillText(LanguageStrings.getString("language"), 120, 340);
		Mag2DScreen.fillText(LanguageStrings.getString("show"), 120, 422);
		Mag2DScreen.fillText(LanguageStrings.getString("prompt"), 120, 504);
		
		if (this.backTime == 0)
			Mag2DScreen.drawImage(MusicalBottles.backButton, 16, MusicalBottles.yBack);
		else {
			if ((this.getTime() - this.backTime) > 300) {
				this.backTime = 0;
				Mag2DScreen.drawImage(MusicalBottles.backButton, 16, MusicalBottles.yBack);
				// code taken from plant nursery
				options.store();
				options.apply();
				//console.log("tickbox is "+options.showPrompt);
				MusicalBottles.storeLanguageSetting();
				document.title=LanguageStrings.getString("title");
				options.RemoveHTML();			
				this.Location = "home";
				this.doRender();
			
				MusicalBottles.Location = "home";	
			} else
				Mag2DScreen.drawImage(MusicalBottles.backDownButton, 16, MusicalBottles.yBack);
		}	
		//ctx.drawImage(Sorting.BackButton(), 10, 714);
		//ctx.drawImage(Sorting.FwdButton(), 978, 714);		
	},
	

			
	renderReports: function() {
		var ctx = this.getCanvasContext();
		
		Mag2DScreen.drawImage(MusicalBottles.GameImages["instructions"]["image"], 0, 0);
		Mag2DScreen.drawImage(MusicalBottles.GameImages["ipanel"]["image"], 0, 0);
		//ctx.drawImage(MusicalBottles.GameImages["lozenge_naked"]["image"], 430, MusicalBottles.yBack-12);	// Lozenge for "Reset"		

		this.Reporting.render(Mag2DScreen, this.CanvasWidth, this.CanvasHeight);
		
		if (this.resetTime == 0)
			MusicalBottles.rc = 0;
		else {		
			// draw the recycle button turning
			var date = new Date();
			var turnTime = 200;		
			var currentTime = date.getTime();
			if ((currentTime - this.resetTime) < 1000) {  // only rotate the recycle icon for 10 secs
				if ((currentTime - this.recycleTime) > turnTime) {
					MusicalBottles.rc++;	
					if (MusicalBottles.rc > 3)
						MusicalBottles.rc = 0;
					this.recycleTime = currentTime;
				}
			} else {
				this.resetTime = 0;
			}
		}
		//console.log("rep "+this.resetTime+" "+(currentTime - this.recycleTime));
		//if (!MusicalBottles.adMob)
			Mag2DScreen.drawImage(MusicalBottles.recycle[MusicalBottles.rc], (512 + 61), 646 - MusicalBottles.adMobHeightAdjust);
		//else
		//	Mag2DScreen.drawImage(MusicalBottles.recycle[MusicalBottles.rc], (512 + 61), 526);
		//var s = "" + MusicalBottles.rc;
		//ctx.fillText(s, (512 - 39), 674);
			
		if (this.backTime == 0)
			Mag2DScreen.drawImage(MusicalBottles.backButton, 16, MusicalBottles.yBack);
		else {
			if ((this.getTime() - this.backTime) > 300) {
				this.backTime = 0;
				Mag2DScreen.drawImage(MusicalBottles.backButton, 16, MusicalBottles.yBack);
				MusicalBottles.Location = "home";	
			} else
				Mag2DScreen.drawImage(MusicalBottles.backDownButton, 16, MusicalBottles.yBack);
		}
		},
	
    // display one of the controls on the home page	
    showControl: function(num, text) {
			var xb = 774; var wb = 214;
			
		y0 = HomescreenClickZones[text].top; // 105
		
		var offs = 0;
		// test if button has been pressed
		if (this.homeTime[num] > 0) {
			// test if button has been down long enough
			if ((this.getTime() - this.homeTime[num]) > 300) {
				this.homeTime[num] = 0;
				offs = 0;
				MusicalBottles.respondToHomeClick(text);
			} else {
				offs = 4;
			}
		}
		// ok to go here as it renders even when a button responded to
		Mag2DScreen.drawImage(this.homeControls[num], 760 + offs, y0 + offs);		
		var s = LanguageStrings.getString(text);
		x = (wb - Mag2DScreen.textWidth(s))/2;
		Mag2DScreen.fillText(s, xb + x + offs, y0+48+offs); //153
		//console.log("2) "+ s + " "+xb+" "+x+" "+Mag2DScreen.textWidth(s));		
	},
		
	renderHome: function () {
		var ctx = Mag2DScreen; //MusicalBottles.getCanvasContext();
		var lang = LanguageStrings.CurrentLocale.substring(0, 2);
		var createHomescreen = true; // false when we have a pre-drawn screen
		
		var name = "bg_"+LanguageStrings.getLocale();
		Mag2DScreen.drawImage(MusicalBottles.GameImages[name]["image"], 0, 0);
			
		ctx.font = "28px andika_basicregular";
		Mag2DScreen.font = "28px andika_basicregular";
		Mag2DScreen.setFont2(28,"andika_basicregular");
		ctx.fillStyle = "white";
			
        if (createHomescreen) {				
			ctx.fillStyle = "white";
			Mag2DScreen.setFillStyle("white");
			var xb = 774; var wb = 214;
			
			// output options
			this.showControl(4, "options");
			/*y0 = HomescreenClickZones["options"].top; // 105
			Mag2DScreen.drawImage(this.homeControls[4], 760, y0);
			var s = LanguageStrings.getString("options");
			x = (wb - Mag2DScreen.textWidth(s))/2;
			Mag2DScreen.fillText(s, xb + x, y0+48); //153
			//console.log("2) "+ s + " "+xb+" "+x+" "+Mag2DScreen.textWidth(s));*/			

			// output start
			this.showControl(0, "start");
			/*y0 = 105; //HomescreenClickZones[prop].top; // 205
			Mag2DScreen.drawImage(this.homeControls[0], 760, y0);
			s = LanguageStrings.getString("start");
			tm = Mag2DScreen.measureText(s);
			x = (wb - Mag2DScreen.textWidth(s))/2;
			Mag2DScreen.fillText(s, xb + x, y0+48); //253
			//console.log(s + " "+xb+" "+x+" "+Mag2DScreen.textWidth(s));*/
		
			// output instructions
			this.showControl(1, "instructions");
			/*y0 = HomescreenClickZones["instructions"].top; // 300
			Mag2DScreen.drawImage(this.homeControls[1], 760, y0);			
			s = LanguageStrings.getString("instructions");
			tm = Mag2DScreen.measureText(s);
			x = (wb - Mag2DScreen.textWidth(s))/2;			
			Mag2DScreen.fillText(s, xb + x, y0+48); //349
			//console.log(s + " "+tm.width+" "+x+" "+Mag2DScreen.textWidth(s)+" "+Mag2DScreen.originalX(100));*/

			// output reports
			this.showControl(2, "reports");
			/*y0 = HomescreenClickZones["reports"].top; // 400
			Mag2DScreen.drawImage(this.homeControls[2], 760, y0);
			s = LanguageStrings.getString("reporting");
			tm = Mag2DScreen.measureText(s);
			x = (wb - Mag2DScreen.textWidth(s))/2;	
			Mag2DScreen.fillText(s, xb + x, y0+48); // 446*/
			
			// title
			s = LanguageStrings.getString("title");
			tm = Mag2DScreen.measureText(LanguageStrings.getString("title"));
			x = (770 - Mag2DScreen.textWidth(s)/*tm.width*/)/2;			
			Mag2DScreen.fillText(LanguageStrings.getString("title"), x, 180);
			//console.log(s + " "+Mag2DScreen.textWidth(s));			
		}
		Mag2DScreen.setFont2(28,"andika_basicregular"); // reset the font size
		// display logo
		Mag2DScreen.drawImage(this.logoImage, 816, 490);	// 770, 639 to center	
		
	},
	
	
	LanguageChanged: function() {
		var language = document.getElementById('options_language');
		options.locale = language.options[language.selectedIndex].value;
		LanguageStrings.setLocale(options.locale);
		if (options.locale != options.oldLocale) {
			MusicalBottles.loadBitmaps(false);
		}
		this.updateOptionsScreen();
		this.doRender();
	},
	
	updateOptionsScreen: function() {
		var difficultySelector = document.getElementsByName('options_difficulty');
		var soundDiv = document.getElementById('options_sound');
		var language = document.getElementById('options_language');
		
		for (i=0; i<language.options.length;i++) {
			language.options[i].text = LanguageStrings.getString(options.locales[i].split('_')[0]);
			if ( language.options[i].value === options.locale) {
				language.selectedIndex = i;
				console.log("selected locale index is: " + i); 
			}
		}
				
		for (i=0;i<difficultySelector.length;i++) {
			document.getElementById(difficultySelector[i].value+"_label").innerHTML=LanguageStrings.getString(difficultySelector[i].value);
			
			if(difficultySelector[i].value == options.difficulty) {
				difficultySelector[i].checked = true;
			} else {
				difficultySelector[i].checked = false;
			}
		}
	},

	traceOn: true,
	renderTextInBox: function(s, left, width, y) {

		var ctx = MusicalBottles.getCanvasContext();
		var tm = Mag2DScreen.measureText("hy");
		var hh = tm.height;
		var flag = false;
		var prevSpace = 0;
		var s2 = s;

		
		do {
			var toowide = false;
			do {
			    // find next space, % or end of string
			    var nx_sp = s2.indexOf(" ");
			    var nx_pc = s2.indexOf("%");

			    //console.log("tra "+nx_sp+" "+nx_pc+" "+(prevSpace+(nx_sp))+" "+s2);				
			    if ((nx_sp != -1) || (nx_pc != -1)) {
					// end not reached
				    if ((nx_pc != -1) && (nx_pc < nx_sp)) {
				       var str = s.slice(0,prevSpace + nx_pc);
					   //console.log("outputted "+s);
				       Mag2DScreen.fillText(str, left, y);
				       y += 24;	
				       s = s.slice(prevSpace + nx_pc+1);
					   s2 = s;
					   prevSpace = 0;					   
					} else {
			            // assume space. test if it fits on a line
				        var str = s.slice(0,prevSpace + nx_sp);
				        var tm = Mag2DScreen.measureText(str);
						//	bizarrely, we test for it being less than the width, and don't output then
						//  but it works! if a problem, make the 'width' param slightly smaller!
				        if (Mag2DScreen.textWidth(str) /*tm.width*/ < width) {
				          toowide = false;
						  //console.log(str+" is not wide enuf "+Mag2DScreen.textWidth(str));
					      s2 = s2.slice(nx_sp+1);
					      prevSpace = prevSpace + (nx_sp+1);
				        } else {
				          // output line
				          var str2 = s.slice(0,prevSpace + nx_sp);
				          Mag2DScreen.fillText(str2, left, y);
				          y += 24;
				          //if (MusicalBottles.traceOn) console.log("else "+Mag2DScreen.textWidth(str) +" "+str+" "+width);
				          s = s.slice(prevSpace + nx_sp+1);
					      s2 = s;
					      prevSpace = 0;
					   }						  
				   }
				} else {
				   // end reached
			       toowide = true; flag = true;
				   Mag2DScreen.fillText(s, left, y);				   
				}
			} while (!toowide);

		} while (!flag);
		MusicalBottles.traceOn = false;
	},
	
	renderInstructions: function () {
		var ctx = MusicalBottles.getCanvasContext();
		//console.log("renderInstructions "+MusicalBottles.GameImages["instructions"]["image"]);
		Mag2DScreen.drawImage(MusicalBottles.GameImages["instructions"]["image"], 0, 0);
		Mag2DScreen.drawImage(MusicalBottles.GameImages["ipanel"]["image"], 0, 0);
		
		var left = 60;
		var width = 800;
		var y = 100;
		var s = LanguageStrings.getString("instruction_text");
		//var s2 = s;
		var i = 0; // character in string - it can be truncated

		
		// display the title
		s = LanguageStrings.getString("instructions");
		Mag2DScreen.font = "26px Andika Basic";	
		Mag2DScreen.fillStyle = "black";
		Mag2DScreen.fillText(s, left, y - 40);
		
		s = LanguageStrings.getString("instruction_text");
		Mag2DScreen.font = "22px Andika Basic";	
					Mag2DScreen.setFont2(22,"andika_basicregular");
		Mag2DScreen.fillStyle = "black";
		Mag2DScreen.setFillStyle("black");		
		MusicalBottles.renderTextInBox(s, left, width, y);

		if (this.backTime == 0)
			Mag2DScreen.drawImage(MusicalBottles.backButton, 16, MusicalBottles.yBack);
		else {
			if ((this.getTime() - this.backTime) > 300) {
				this.backTime = 0;
				Mag2DScreen.drawImage(MusicalBottles.backButton, 16, MusicalBottles.yBack);
				MusicalBottles.Location = "home";	
			} else
				Mag2DScreen.drawImage(MusicalBottles.backDownButton, 16, MusicalBottles.yBack);
		}
		//ctx.drawImage(MusicalBottles.GameImages["lozenge"]["image"], 368, 718 - MusicalBottles.iPadHeightAdjust - MusicalBottles.adMobHeightAdjust);				
	},
	

		

	shuffleOffSineWave: function (num) {
		// 
		if (num != 0) console.log("Non zeroth element being removed by shuffleOffSineWave");
		if (MusicalBottles.nHits > 0) {
			for (var i=0;i<(MusicalBottles.nHits-1);i++) {
				MusicalBottles.wLength[i] = MusicalBottles.wLength[i+1];
				MusicalBottles.sx[i] = MusicalBottles.sx[i+1];	
				for (var k=0;k<MusicalBottles.outputs[i+1].length;k++)
					MusicalBottles.outputs[i][k] = MusicalBottles.outputs[i+1][k];				
			}
			MusicalBottles.nHits--;
			MusicalBottles.wLength[MusicalBottles.nHits] = 0;
			MusicalBottles.sx[MusicalBottles.nHits] = 0;
			for (var k=0;k<MusicalBottles.outputs[MusicalBottles.nHits].length;k++)
					MusicalBottles.outputs[MusicalBottles.nHits][k] = 0;
			//console.log("shuffleOffSineWave called, nHits now "+MusicalBottles.nHits);
		}

	},

	sb_clicked: false,
	sb_filled: false,
	sb_emptied: false,
	sb_recorded: false,
	
    // states for prompt
	showPrompt: function () {
		// show some words at the bottom of the screen to help the user )if turned on)
		
		var xs = 200;
		var ys = 500;
		if (MusicalBottles.adMob) ys -=30;
		var ctx = MusicalBottles.getCanvasContext();		
				
		Mag2DScreen.drawImage(MusicalBottles.GameImages["speech"]["image"], xs, ys);	
		ctx.font = "22px andika_basicregular";
		Mag2DScreen.setFont2(28,"andika_basicregular");
		var text = "";
		if (MusicalBottles.Recording)
			text = "prompt_stoprec"; //"Press STOP to stop the recording";
		else if (MusicalBottles.Playback)
			text = "prompt_clickplay"; //"You can play along by clicking on the bottles!";
		else if (MusicalBottles.sb_recorded && MusicalBottles.sb_filled && MusicalBottles.clicked && !MusicalBottles.sb_emptied)
			text = "prompt_emptybottle"; //"You can empty the bottle at any time";
		else  if (MusicalBottles.sb_filled && MusicalBottles.sb_clicked && (MusicalBottles.Bottles.length == 5) && !MusicalBottles.sb_recorded)
			text = "prompt_rtorecord"; //"Press R to record the tune you create";
		else  if (MusicalBottles.sb_filled && MusicalBottles.sb_clicked && (MusicalBottles.Bottles.length < 5))
			text = "prompt_addbottles"; //"You can add more bottles by pressing the button with + on it";
		else if (MusicalBottles.sb_clicked && !MusicalBottles.sb_filled)
			text = "prompt_fillwater"; //"Try filling the bottle with some water";
		else if (MusicalBottles.Bottles.length == 1)
			text = "prompt_clicksound"; //"Click the mouse on the bottle to hear the sound it makes";
		else
			text = "prompt_clicksound"; //"Click the mouse on any bottle to hear the sound it makes";	
		
		//ctx.fillText(text, xs+80, ys+80);
		Mag2DScreen.fillStyle = "black";
		Mag2DScreen.setFillStyle("black");			
		MusicalBottles.renderTextInBox(LanguageStrings.getString(text), xs+65, 150, ys+68);
		//console.log(" show prompt "+options.showPrompt+" "+MusicalBottles.sb_filled+" "+ MusicalBottles.sb_clicked+" "+ MusicalBottles.sb_recorded+" "+MusicalBottles.Jug.anim_index+" "+text);
	},
	
	renderGame: function () {
		//console.log("Rendering game");
		
		var YOffset = 54; // - this.adMobHeightAdjust/2;
		if (MusicalBottles.adMob)
			YOffset = 0;
		
		var s, i;
		
		var ctx = MusicalBottles.getCanvasContext();

		Mag2DScreen.drawImage(cfg.topics[MusicalBottles.CurrentTopic]["image"], 0, 0);		// to avoid top of screen being uncovered

        // Draw music and notes
		var sheetmusicRect = MusicalBottles.sheetmusicRect;
			
		var x0 = MusicalBottles.sheetmusicRect.left + 5;
		var y0 = MusicalBottles.sheetmusicRect.top + 74;
		var mx = 60; // max y -height
		var i = 0;

		var rStartRandom = new Array(300);

		if (options.showMusic == "playnote") {
		  Mag2DScreen.drawImage(MusicalBottles.GameImages["sheetmusic"]["image"], MusicalBottles.sheetmusicRect.left, MusicalBottles.sheetmusicRect.top);
		  var outs = "Notes "+MusicalBottles.CurrentNoteIndex + " " + MusicalBottles.MusicalNotes.length+ " : ";
		  // currently not sure why there is a different start point for recording and playback
		  var start = 0; //MusicalBottles.CurrentNoteIndex;
		  var finish = MusicalBottles.MusicalNotes.length;
		  if (MusicalBottles.Playback) {
		    start = 0; finish = MusicalBottles.PlaybackIndex; //CurrentNoteIndex;
		  }
		  if (finish > (start + 8)) {
		    start = finish - 8;		// only display the last 8 notes
		  }
		  //outs = outs + "s/f "+start + " " + finish;
		  var xp = x0 + 25; //MusicalBottles.initialNoteX - MusicalBottles.noteOffset;
		  var i = 0;
		  var noteToShow = 0; // show gradually more shaded notes
		  for (var note_index = start; note_index < finish; note_index++) {
			var note = MusicalBottles.MusicalNotes[note_index];
			note.x = MusicalBottles.initialNoteX + (note_index)*MusicalBottles.noteOffset;
			xp += MusicalBottles.noteOffset;			
			//note.x = MusicalBottles.initialNoteX + (note_index-MusicalBottles.CurrentNoteIndex)*MusicalBottles.noteOffset;			
			Mag2DScreen.drawImage(this.showNotes[noteToShow], xp, y0 + 1  - note.y);
			//console.log("notes "+note_index+" "+i+" "+note.y+" "+noteToShow);
			i++;
			if ((i % 2) == 0) noteToShow++;
		  }
		} else if (options.showMusic == "sinewave") {
		  y0 -= 24;
		  Mag2DScreen.drawImage(MusicalBottles.GameImages["sinewave"]["image"], MusicalBottles.sheetmusicRect.left, y0 - 74 /*MusicalBottles.sheetmusicRect.top*/);
		  Mag2DScreen.beginPath();	  
		  Mag2DScreen.setStrokeStyle('#ff0000');		  

			//console.log("start/end "+ MusicalBottles.sx[0]+" "+(MusicalBottles.sx[0] + MusicalBottles.wLength[0])+" "+MusicalBottles.outputs[0][10]);
			for (i=0;i<204;i++) {
				var y = 0;
				for (j=0;j<MusicalBottles.nHits;j++) {

					if (i > MusicalBottles.sx[j] && (i < (MusicalBottles.sx[j] + MusicalBottles.wLength[j]))) {
						y = y + MusicalBottles.outputs[j][i - MusicalBottles.sx[j]];
						if (y > mx) y = mx;
						if (y < -mx) y = -mx;						
						//console.log("wave "+i+" "+MusicalBottles.outputs[j][10]);
					}
				}
				y += 6 * Math.random();
				if (i == 0)
				  Mag2DScreen.lineTo(x0+i, y0-y);
				else
				  Mag2DScreen.lineTo(x0+i, y0-y);			
			}
			Mag2DScreen.stroke();
			for (j=0;j<MusicalBottles.nHits;j++) {			
				MusicalBottles.sx[j] += 2;
				if (MusicalBottles.sx[j] >= 204) // this sine wave has gone off the right hand side, remove it
					MusicalBottles.shuffleOffSineWave(j);
			}		
		}
		//console.log(outs);
		
		// draw recording in progress
		if (MusicalBottles.Recording) {
			Mag2DScreen.setFont2(28,"andika_basicregular");
			Mag2DScreen.fillStyle = "white";
			Mag2DScreen.setFillStyle("white");
			for (var j=0;j<3;j++) {
			   s = LanguageStrings.getString("recording"+j);
			   var xp = 512 - (Mag2DScreen.textWidth(s)/2); // uses new function
		       Mag2DScreen.fillText(s, xp, 80 + 45*j);
			}
		}
	
        // draw the scientist
		var date = new Date();
		var currentTime = date.getTime();
		var closeTime = 3000;
		var reOpenTime = 3200;
		var scientist = 0;		

		if ((currentTime - this.blinkTime) > closeTime) {
			scientist = 1;
			if ((currentTime - this.blinkTime) > reOpenTime) {
			   scientist = 0; this.blinkTime = date.getTime();
			}
		}
		var ys = 440;
		if (MusicalBottles.adMob) ys -=20;
		Mag2DScreen.drawImage(this.animScientist[scientist], 10 , ys);	
		if (options.showPrompt)
			MusicalBottles.showPrompt();
		
		var flameTime = 90;
		if ((currentTime - this.flameTime) > flameTime) {
			nFlame++;
			if (nFlame > 2) nFlame = 0;
			this.flameTime = currentTime;
		}
		Mag2DScreen.drawImage(this.animFlame[nFlame], 893, 174);
		// draw the flame
        
        // Render bottles
        var nBottles = MusicalBottles.Bottles.length;
        for (var it = 0; it < nBottles; it++) {
            var bottle = MusicalBottles.Bottles[it];
            Mag2DScreen.drawImage(bottle.image, bottle.rect.left, bottle.rect.top);
			//console.log("bottle at "+bottle.button.state);
            var button = bottle.button;  
            

            // Default button state is up
            var img = button.up;      
            if (button.state === 'down') {
                img = button.down;  
            }
			if (button.state === 'disabled') { // ie empty
                img = button.grey;  
            }

            Mag2DScreen.drawImage(img, button.rect.left, button.rect.top);


        }
		
		/* Draw the topic title
		s = cfg.topics[MusicalBottles.CurrentTopic].title; // a reference eg title0-5
		s = LanguageStrings.getString(s);	
		var place = LanguageStrings.getString("place");
		ctx.font = "32px Andika Basic";
		var tm = ctx.measureText(s);
		var placewidth = ctx.measureText(place);
		
		ctx.fillStyle = "rgba(0, 0, 0, 0.7)";
		var titleleft = (MusicalBottles.CanvasWidth-tm.width)/2;
		var placeleft = (MusicalBottles.CanvasWidth-placewidth.width)/2;
		MusicalBottles.roundedRect(ctx, placeleft-20, -10, placewidth.width+40, 80, 10);
		

		ctx.fillStyle = "white";
		ctx.textBaseline = "top";
		ctx.fillText(s, (MusicalBottles.CanvasWidth-tm.width)/2, 2);
		//if (!MusicalBottles.adMob) {
		  ctx.fillText(place, placeleft, 35);
		//}*/
		Mag2DScreen.textBaseline = "alphabetic";		
		Mag2DScreen.fillStyle = "black";

		// render all buttons
		var outs = "mp ";
		outs = outs + " "+MusicalBottles.Buttons[0].name +" " + MusicalBottles.Buttons[0].state;

		var i = 0;
			
            for (var button in cfg.topics[0].buttons) {	//for (var button in MusicalBottles.Buttons) {	
                var b = cfg.topics[0].buttons[button];	//var b = MusicalBottles.Buttons[button];
				var buttonData = MusicalBottles.Buttons[button];
				
				//console.log("button "+b.name+" "+b.state);
				  if (b.name != "empty_bottle") { // don't display the icon for empty bottle, as it's one per bottle & done elsewhere
				   if (b.state === 'disabled') {
					Mag2DScreen.drawImage(cfg.topics[0].buttons[button].grey, b.rect.left, b.rect.top);	
					//outs = outs+ "dis "+b.name;
					//ctx.fillText("dis"+b.name+" ", 500, b.rect.top);					
				   } else {
						outs = outs+ "a"+ b.name+" "+b.state;
					if (b.state === 'down') {
						outs = outs+" xdown";
						//Mag2DScreen.drawImage(cfg.topics[0].buttons[button].down, b.rect.left, b.rect.top);
						if ((currentTime - b.downTime) > 300) { // was 300
							b.state = 'up';
							// take action if play/stop/pause/record or back buttons pressed
							if (b.name == "record") {
								MusicalBottles.StartRecording();
							} else if (b.name == "back") {
								//if (bottleFilling > -1)
								//  MusicalBottles.StopFillingBottle(bottleFilling);
								Mag2DScreen.drawImage(cfg.topics[0].buttons[button].up, b.rect.left, b.rect.top);
								MusicalBottles.Location = "home";
								var date = new Date();
								MusicalBottles.ActivityDuration = Math.floor((500 + date.getTime() - MusicalBottles.ActivityStart)/1000); // rounded to nearest second
								//console.log("times "+	MusicalBottles.ActivityStart + "  "+ MusicalBottles.ActivityDuration);					
								MusicalBottles.Reporting.incValue(cfg.topics[0].reports[MusicalBottles.REP_DURATION].name, "Correct", MusicalBottles.ActivityDuration);						
								MusicalBottles.storeLanguageSetting();
								MusicalBottles.StopPlayback();
								this.stopAllAudio();
							} else if (b.name == "stop") {
								if (MusicalBottles.Recording) {
									MusicalBottles.StopRecording();
								} else {
									MusicalBottles.StopPlayback();
								}
							} else if (b.name == "play") {
								MusicalBottles.StartPlayback();
							} else if (b.name == "pause") {
								MusicalBottles.PausePlayback();
							}								
						}
						// if the button has just gone up, it's drawn above, and two images can be shown!
						if (b.state === 'down')
						  Mag2DScreen.drawImage(cfg.topics[0].buttons[button].down, b.rect.left, b.rect.top);							
						outs = outs + " "+b.state +((currentTime - b.downTime)); //+buttonData.name;
					} else {
						Mag2DScreen.drawImage(cfg.topics[0].buttons[button].up, b.rect.left, b.rect.top);
						//MusicalBottles.buttonPressedTime[button] = 0;
					}
					MusicalBottles.buttonPressedTime[button] = currentTime;

					//ctx.fillText("ok"+b.name+" ", 500, b.rect.top);
				   }
				  } // if it's the empty bottle button
			}
		if (!this.helpAudioPlaying)	{
		  Mag2DScreen.drawImage(this.GameImages["audioOn"]["image"], this.icnAudio.x, this.icnAudio.y);
	      //outs = outs + "helpa";
	    } else {
		  Mag2DScreen.drawImage(this.GameImages["audioPlay"]["image"], this.icnAudio.x, this.icnAudio.y);
		  //outs = outs + "helpb "+this.soundPlaying;
		  if (this.soundPlaying == 2) {
			  this.soundPlaying = 0;
			  //outs = outs +"ended";
			  this.helpAudioPlaying = false;
		  }

		}
		//console.log(outs);
		//console.log("mouse over bottle "+MusicalBottles.xdrumstick+" "+MusicalBottles.ydrumstick);	
		if (this.xdrumstick > -1) {
			Mag2DScreen.drawImage(this.drumstick, this.xdrumstick, this.ydrumstick - 50);
		}
           
		Mag2DScreen.drawImage(MusicalBottles.Jug.image, MusicalBottles.Jug.rect.left, MusicalBottles.Jug.rect.top);
		if (MusicalBottles.Jug.anim_index == 4) {
			Mag2DScreen.fillStyle = "#94CCD6"; //"white";  // 
			var h = 360 - (MusicalBottles.Jug.rect.top + 250); 
			Mag2DScreen.fillRect(MusicalBottles.Jug.rect.left + 5, MusicalBottles.Jug.rect.top + 250, 10, h);
			console.log("Anim Jug "+MusicalBottles.Jug.anim_index+" "+MusicalBottles.Jug.rect.top + " "+h);
		}
        for (var bottle_index in MusicalBottles.Bottles) {
            var bottle = MusicalBottles.Bottles[bottle_index];
			//ctx.rect(bottle.rect.left,bottle.rect.top-75,bottle.rect.width,bottle.rect.height+75);
			//ctx.stroke(); 
		}
		//ctx.fillText("mx "+MusicalBottles.mx+" "+MusicalBottles.my, 10, 100);
		
		//ctx.drawImage(this.backButton, 10, 514); //console.log("output backbutton");			
	},
			

    SlideSmudge: function() {
        if (MusicalBottles.smudgeDirection > 0) {
            MusicalBottles.Smudge.rect.top -= 20;
        } else if (MusicalBottles.smudgeDirection < 0) { 
            MusicalBottles.Smudge.rect.top += 20;
        } 
		if (MusicalBottles.smudgeDirection !== 0) {
			if (MusicalBottles.Smudge.rect.top >= MusicalBottles.Smudge.maxTop) {
				clearInterval(MusicalBottles.Smudge.interval); 
				MusicalBottles.Smudge.interval = null;
			}
		}
        //console.log("Slide SMudge");
    }, 
	
	//
	// --- RESPOND TO MOUSE CLICKS 1 (General) Functions ---
	//
				
	hand: null,
	handOffsetX: 0,
	handOffsetY: 0,
	mx: 0,
	my: 0,

	// reads the x,y position of mouse click or touch on a tablet	
	mousePosition: function(event) {

		if (!MusicalBottles.isAndroid) {
			this.xpsn = event.layerX;
			this.ypsn = event.layerY;
		
			if (event.offsetX!==undefined) {
				this.xpsn = event.offsetX;
				this.ypsn = event.offsetY;
			}
		} else {
			// Android solution
			if (touchend) {
				// for Android, no data with touch end, so use last known position
				x = MusicalBottles.hand.CurrentX + MusicalBottles.handOffsetX;
				y = MusicalBottles.hand.CurrentY + MusicalBottles.handOffsetY;
			} else {
				var touch = event.touches[0];
				this.xpsn = touch.pageX;
				this.ypsn = touch.pageY;
			}
		}
		// convert the units to original co-ords (as buttons defined in these terms)
		//console.log("WAS "+this.spsn+" will be "+Mag2DScreen.originalX(this.xpsn));
		this.xpsn = Mag2DScreen.originalX(this.xpsn);
		this.ypsn = Mag2DScreen.originalX(this.ypsn);		
	},	
	

	// callback when mouse clicked (or screen touched)
	ev_mousedown: function (event) {
		
		MusicalBottles.mousePosition(event, false); x = MusicalBottles.xpsn; y = MusicalBottles.ypsn;
		// convert the units to original co-ords (as buttons defined in these terms)
		//console.log("WAS "+x+" "+y);

		//console.log("canvasclicked");
		switch (MusicalBottles.Location) {
			case "home":
				MusicalBottles.homeClicked(x, y);
				break;
				
			case "instructions":
				MusicalBottles.instructionsClicked(x, y);
				break;
				
			case "reports":
				MusicalBottles.reportsClicked(x, y);
				break;
			
			case "options":
				MusicalBottles.optionsClicked(x, y);
				break;
				
			case "game":
				MusicalBottles.gameClicked(x, y);
				MusicalBottles.gameClicked2(event);
				break;
			default: 
				break;
		}
	},

	//cursor_drumstick: 'url(' + window.location.href.split('musicalbottles.html')[0] + "images/drumstick.gif" + '), auto',	
	cursor_drumstick: 'none',
	cursor_normal: 'auto',
	bottleFilling: null,
	notOverAnyBottle: true,
	
	// callback when the mouse is moved (dragging or not)
	ev_mousemove: function(event) {
		/*if (MusicalBottles.hand == null)
		  console.log("ev_mouemove: hand is null");
		else
		  console.log("ev_mouemove: hand is not null");*/
		if (MusicalBottles.Location !== "game") {
			return;
		}
		var x = 0;
		var y = 0;
		MusicalBottles.mousePosition(event, false); x = MusicalBottles.xpsn; y = MusicalBottles.ypsn;	
		
		if (MusicalBottles.hand === null) {
			var overAnyBottle = false;
			for (var bottle_index in MusicalBottles.Bottles) {
				var bottle = MusicalBottles.Bottles[bottle_index];
				//console.log("bottle# "+bottle_index+" "+bottle.rect.left);
				 if ((x > bottle.rect.left && x < bottle.rect.left + bottle.rect.width) && 
				     (y > bottle.rect.top && y < bottle.rect.top + bottle.rect.height) && 
					 (MusicalBottles.Jug.state != 'pouring'))								// don't show drumsticks if we are pouring
				 { 		
					overAnyBottle = true;
				 } 
			}
			if (overAnyBottle) {
			    document.getElementById("gamecanvas").style.cursor = MusicalBottles.cursor_drumstick; // turn cursor off (also works on IE)
			    MusicalBottles.xdrumstick = x; MusicalBottles.ydrumstick = y;			
				//console.log("drumstick "+y);				
			} else {
			    document.getElementById("gamecanvas").style.cursor = MusicalBottles.cursor_normal; // turn cursor back on
				MusicalBottles.xdrumstick = -1; MusicalBottles.ydrumstick = -1;	
				//console.log("normal "+y);				
			}
			return
		} else {
			//console.log("hand is not null "+MusicalBottles.Jug.state);
			MusicalBottles.notOverAnyBottle = true;
            //if ( MusicalBottles.Jug.state === 'held') {
				for (var bottle_index in MusicalBottles.Bottles) {
					var bottle = MusicalBottles.Bottles[bottle_index];
					var button = bottle.button; 

					var xl = 0;//x - MusicalBottles.handOffsetX;
					var yl = 0; //y - MusicalBottles.handOffsetY + 216; // bot left corner of jug
					if (MusicalBottles.hand != null) {
						xl = MusicalBottles.hand.rect.left;
						yl = MusicalBottles.hand.rect.top+216;
					} // end of ios code

					if (MusicalBottles.hand !== null) {
					  console.log("up x: "+xl+" "+ Math.round(xl - bottle.rect.left) +" right "+ Math.round((bottle.rect.left + bottle.rect.width) - xl)+" y "+MusicalBottles.hand.rect.top);
					  if ((xl > (bottle.rect.left+10) && xl < (bottle.rect.left + bottle.rect.width-40)) && // was - 30
						(/*yl > (bottle.rect.top - 75) &&*/ yl < bottle.rect.top + bottle.rect.height)) { 
						    MusicalBottles.notOverAnyBottle = false;
						    MusicalBottles.bottleFilling = bottle;
						    console.log("in position "+MusicalBottles.Jug.state+" "+bottle.interval);
						    if ( MusicalBottles.Jug.state === 'held') {
							    if (bottle.interval === null) {
								    MusicalBottles.FillBottle(bottle);
							    }
						    }
					    } //else {}
				        /*if (MusicalBottles.notOverAnyBottle && (MusicalBottles.bottleFilling != null)) {
							MusicalBottles.Jug.anim_index = 0; // em stop
					        MusicalBottles.StopFillingBottle(MusicalBottles.bottleFilling);

				        }*/
				        console.log("stop filling "+MusicalBottles.notOverAnyBottle+" "+MusicalBottles.bottleFilling+" "+x);
                    }
			        console.log("mouse move "+MusicalBottles.Jug.state+" "+MusicalBottles.notOverAnyBottle+" "+x);
                }
				if (MusicalBottles.notOverAnyBottle && (MusicalBottles.bottleFilling != null)) {
						MusicalBottles.Jug.anim_index = 0; // em stop
					    MusicalBottles.StopFillingBottle(MusicalBottles.bottleFilling);
						MusicalBottles.AnimateJug(MusicalBottles.bottleFilling);

				}
            //}
	
		}

		var hMenuBar = 62; // note this is slightly different elewhere
		
		// don't let item disappear under adMob banner
		if (y > (MusicalBottles.CanvasHeight - MusicalBottles.adMobHeightAdjust - hMenuBar)) {
			y =  MusicalBottles.CanvasHeight - MusicalBottles.adMobHeightAdjust - hMenuBar;
		}
		
		MusicalBottles.hand.rect.left = x - MusicalBottles.handOffsetX;
		MusicalBottles.hand.rect.top = y - MusicalBottles.handOffsetY;
		
		if (MusicalBottles.hand.rect.top > 70) {
			MusicalBottles.hand.rect.top = 70;
		}
		
		if (MusicalBottles.hand.rect.top > MusicalBottles.CanvasHeight - MusicalBottles.adMobHeightAdjust - hMenuBar - MusicalBottles.hand.rect.height) {
			MusicalBottles.hand.rect.top = MusicalBottles.CanvasHeight - MusicalBottles.adMobHeightAdjust - hMenuBar - MusicalBottles.hand.rect.height;
		} else if (MusicalBottles.hand.rect.top < 0) {
			MusicalBottles.hand.rect.top = 0;
		}
		
		if (MusicalBottles.hand.rect.left > MusicalBottles.CanvasWidth - MusicalBottles.hand.rect.width) {
			MusicalBottles.hand.rect.left = MusicalBottles.CanvasWidth - MusicalBottles.hand.rect.width;
		} else if (MusicalBottles.hand.rect.left < 0) {
			MusicalBottles.hand.rect.left = 0;
		}
		
		;
	},
	
	// callback when the mouse button is released
	// ??? on touchscreens?
	ev_mouseup: function (event) {
		//console.log("mouse up " + event.layerX + ", " + event.layerY);

		var x = 0;
		var y = 0;		
		MusicalBottles.mousePosition(event, true); x = MusicalBottles.xpsn; y = MusicalBottles.ypsn;
        
        if (MusicalBottles.Location !== "game") {
            return;   
        }
        
        for (var button_index in MusicalBottles.Buttons) {
            var button = MusicalBottles.Buttons[button_index];
            // Player should have no knowledge of these, don't bother checking for press
        
            if (button.state === 'down') {
                button.state = 'up';
            }
        }
        
        for (var bottle_index in MusicalBottles.Bottles) {
            var bottle = MusicalBottles.Bottles[bottle_index];
            var button = bottle.button; 
            if (button.state === 'down') {
                button.state = 'up';
            }
			// start of ios code
			var xl = 0;//x - MusicalBottles.handOffsetX;
			var yl = 0; //y - MusicalBottles.handOffsetY + 216; // bot left corner of jug
			if (MusicalBottles.hand != null) {
			  xl = MusicalBottles.hand.rect.left;
			  yl = MusicalBottles.hand.rect.top+216;
			} // end of ios code
			//console.log("x1/y1 "+xl+" "+yl); //+" last "+MusicalBottles.hand.rect.left+" "+(MusicalBottles.hand.rect.top+216));
            //console.log("botts "+xl+ " "+bottle.rect.left+" "+(bottle.rect.left + bottle.rect.width)+" y: "+yl+" "+MusicalBottles.hand);
            /*if (MusicalBottles.hand !== null) {
			    console.log("up x: "+xl+" "+ Math.round(xl - bottle.rect.left) +" right "+ Math.round((bottle.rect.left + bottle.rect.width) - xl));
                if ((xl > bottle.rect.left && xl < bottle.rect.left + bottle.rect.width) && 
				    (yl < bottle.rect.top + bottle.rect.height)) { 
                    
                    console.log("in position "+MusicalBottles.Jug.state+" "+bottle.interval);
                    if ( MusicalBottles.Jug.state === 'held') {
                        if (bottle.interval === null) {
                            MusicalBottles.FillBottle(bottle);
                        }
                    }
                } else {
					
				}
            }*/
        }
		//console.log("no pour? "+MusicalBottles.Jug.state+" "+(MusicalBottles.Jug.rect.top+ bottle.rect.height)+" "+(bottle.rect.top - 75));
        /* no longer needed if (( MusicalBottles.Jug.state == 'held') && 
			((MusicalBottles.Jug.rect.top + bottle.rect.height)  > (bottle.rect.top - 75))) {
			MusicalBottles.Jug.rect.top = bottle.rect.top - 278;
			//console.log("adjusted ");
		}*/
        //; does nothing
		
    	
		if (MusicalBottles.bottleFilling != null) {
		//	MusicalBottles.StopFillingBottle(MusicalBottles.bottleFilling);
		}
						
        if (MusicalBottles.hand === null) {
			return;
		}
		MusicalBottles.hand = null;
	
	},

	// callback when the mouse re-enters the screen	
	// probably not used with touch screen devices
	ev_mouseover: function (event) {

	    //console.log("mouse is over the canvas "+event.layerX+" "+event.which);
		
		// whilst i prefer having one return, this is ok if Sorting is not defined
		if (MusicalBottles.Location !== "game" || MusicalBottles.hand === null) {
			return;
		}
		
		if (event.which === 0) { // put the object back at the top		
			MusicalBottles.hand.CurrentX = 20 + (MusicalBottles.hand.found*((1024-40)/7));
			MusicalBottles.hand.CurrentY = MusicalBottles.ObjectInitialTop;
			MusicalBottles.hand = null;
		}	
		
	},
	
	//
	// --- RESPOND TO MOUSE CLICKS 2 (Page related) Functions ---
	//
						
	// take action as a result of mouse clicks on the home page
    respondToHomeClick: function(selected) {
			//console.log("home "+x+" "+y+" "+selected);
		
		switch (selected) {
			case "instructions":
				MusicalBottles.Location = "instructions";
				break;

			case "reports":
				this.Location = "reports";
				MusicalBottles.rc = 0;
				var date = new Date();
				this.recycleTime = date.getTime();
				this.doRender();
				break;
							
			case "options":
			var testOptions = true; //false;
				if (!testOptions) {			// toggle language
					var lang = LanguageStrings.CurrentLocale.substring(0, 2);

					if (lang=="es") {
						LanguageStrings.CurrentLocale = "en_GB";
					} else {
						LanguageStrings.CurrentLocale = "es_ES";
					}
					MusicalBottles.storeLanguageSetting();		
					document.title=LanguageStrings.getString("title");			// change title on browser's menubar (or equivalent)			
					MusicalBottles.playAudio("bottles"+MusicalBottles.soundType);
					MusicalBottles.loadBitmaps();
				} else {
					this.Location = "options";
					this.originalLang = LanguageStrings.getLocale();
					this.originalSoundOn = options.soundEnabled;
					this.originalShowMusic = options.showMusic;
					this.originalShowPrompt = options.showPrompt;
					// confirm audio for the menu
					//console.log("store prompt bef options  is "+options.showPrompt);	
					//MusicalBottles.playSound("audio/bottle974"+MusicalBottles.soundType, null, null, false);				
					options.CreateHTML();
					/*document.getElementById('options_difficulty_container').style.zIndex = 10;
					document.getElementById('options_language').style.zIndex = 10;
					document.getElementById('options_weight').style.zIndex = 10;  
					document.getElementById('options_currency').style.zIndex = 10;*/
					options.updateOptionsScreen();
					this.doRender();
				}
				break;
				
			case "start":
				MusicalBottles.BeginTopic(0);
				break;
				
			default:
				//MusicalBottles.checkTopicClicked(x, y);
				break;
		}	
	},
	
	homeClicked: function (x, y) {
		var selected = null;
		
		// this is just for Instructions and Reports. Init'd at end of ConfigData.js
		
		for (var prop in HomescreenClickZones) {
			var left = HomescreenClickZones[prop].left;
			var top = HomescreenClickZones[prop].top;
			var width = HomescreenClickZones[prop].width;
			var height = HomescreenClickZones[prop].height;
			var audio = HomescreenClickZones[prop].audio;			
			//console.log("play audio 1 "+prop+" "+HomescreenClickZones["options"].top);			
			if (x > left && x < left + width) {
				if (y > top && y < top + height) {
					selected = prop;
					console.log("play audio 2 "+prop);
					// confirm audio for the menu
					MusicalBottles.playSound("audio/"+audio+MusicalBottles.soundType, null, null, false);
					var offs = HomescreenClickZones[prop].offset;
					MusicalBottles.homeTime[offs] = this.getTime();  // start timer for the button being pressed
				}
			}
		}

	},
			
		
	GameItems: null,
	Reporting: null,
	
	setupGame: function () {		
		// push references to the items into an array
		this.GameItems = new Array();
		MusicalBottles.date = new Date();
	
		var max = cfg.topics[this.CurrentTopic].items.length;
		//console.log("adding bottle "+MusicalBottles.setupInitialised);
		if (!MusicalBottles.setupInitialised) {
          MusicalBottles.AddBottle();
		  MusicalBottles.setupInitialised = true;
		}
        
		for (var i=0; i<max; i++) {
			if (cfg.topics[MusicalBottles.CurrentTopic].items[i].name === "jug") {
                MusicalBottles.Jug = application.extend(cfg.topics[MusicalBottles.CurrentTopic].items[i]); 
            } else if (cfg.topics[MusicalBottles.CurrentTopic].items[i].name === "smudge") {
                MusicalBottles.Smudge = application.extend(cfg.topics[MusicalBottles.CurrentTopic].items[i]); 
                MusicalBottles.Smudge.rect.left = MusicalBottles.Bottles[0].rect.left - MusicalBottles.Smudge.rect.width/2 + MusicalBottles.Bottles[0].rect.width/2;
            }
			
		}
        
        for (var i in cfg.topics[MusicalBottles.CurrentTopic].buttons) {
            var button = cfg.topics[MusicalBottles.CurrentTopic].buttons[i];
            if ( (button.name.indexOf("empty") == - 1 ) ){
				if (button.name.indexOf("record") !== -1) {
					button.state = 'up';
				} else if (button.name.indexOf("stop") !== -1) {
					button.state = 'disabled';
				} else if (button.name.indexOf("pause") !== -1) {
					button.state = 'disabled';
				} else if (button.name.indexOf("play") !== -1) {
					button.state = 'disabled';
				} else if (button.name.indexOf("add") !== -1) {
					button.state = 'up';
				} else if (button.name.indexOf("remove") !== -1) {
					button.state = 'disabled';			// by default, only one bottle shown at the start
				}
				//console.log("setup "+button.name+" "+button.state);
				MusicalBottles.Buttons.push(application.extend(button));
            }
        }  
		MusicalBottles.sb_clicked = false;
		MusicalBottles.sb_filled = false;
		MusicalBottles.sb_emptied = false;
		MusicalBottles.sb_recorded = false;      	
	},
   
    smudgeDirection: 1,
	smudgeCallback: null,
	
	BeginTopic : function(t) {
		if (this.toasterTimer!==null) {
			clearTimeout(this.toasterTimer);
		}
		this.Location = "home";
		//this.stopAllAudio();
		//this.stopAllAudio();
		// confirm audio for the menu
		//MusicalBottles.playSound("audio/bottle974"+MusicalBottles.soundType, null, null, false);
		this.doRender();
	
		MusicalBottles.CurrentTopic = t;
		MusicalBottles.Bottles.length = 0;
		MusicalBottles.setupInitialised = false;
        MusicalBottles.setupGame();
		MusicalBottles.Location = "game";
		MusicalBottles.PlaybackIndex = 0;
		MusicalBottles.MusicalNotes = new Array();
		var date = new Date();
		MusicalBottles.ActivityStart = date.getTime(); // record time user started playing the activity
		this.blinkTime = date.getTime();
		this.flameTime = date.getTime(); nFlame = 0;
		for (var button in cfg.topics[0].buttons) {
			button.downTime = this.getTime();
		}

		
		MusicalBottles.outputs = new Array(MusicalBottles.MAXHITS);
		
		for (var i = 0; i < MusicalBottles.MAXHITS; i++) {
			MusicalBottles.outputs[i] = new Array(MusicalBottles.MAXHITS);
		}
		MusicalBottles.nHits = 0;
	},
	
			
	optionsClicked: function (x, y) {
		// exit and store results
		if (x > 16 && x < 66 && y > (MusicalBottles.yBack) && y < (MusicalBottles.yBack+50)) {
			MusicalBottles.backTime = this.getTime();  // start timer for the button being pressed

		}		
		/* functionality moved to render options page
		if (x > 368 && x < 368+290 && y > (718-MusicalBottles.iPadHeightAdjust-MusicalBottles.adMobHeightAdjust) && y < (718-MusicalBottles.iPadHeightAdjust-MusicalBottles.adMobHeightAdjust+50)) {
			options.store();
			options.apply();
			document.getElementById('options_difficulty_container').style.zIndex = 0;
			document.getElementById('options_language').style.zIndex = 0;
			MusicalBottles.Location = "home";
			options.RemoveHTML();
			return;
		}*/
		if (x>this.soundZone.left && x<this.soundZone.left+this.soundZone.width && y>this.soundZone.top && y<this.soundZone.top+this.soundZone.height) {		
			
			options.soundEnabled  = !options.soundEnabled;
			this.doRender();
			return;
		}
	},
			
	instructionsClicked: function (x, y) {
		if (x > 16 && x < 66 && y > (MusicalBottles.yBack) && y < (MusicalBottles.yBack+50)) {
			MusicalBottles.backTime = this.getTime();  // start timer for the button being pressed
			//MusicalBottles.Location = "home";
		}
	},
			
	reportsClicked: function (x, y) {
		console.log("reportsClicked "+x+" "+y);
		if (x > 16 && x < 66 && y > (MusicalBottles.yBack) && y < (MusicalBottles.yBack+50)) {
			MusicalBottles.backTime = this.getTime();  // start timer for the button being pressed
			//MusicalBottles.Location = "home";
			return;
		}
		
		var yt = 646 - MusicalBottles.adMobHeightAdjust;
		//if (MusicalBottles.adMob) yt -= 80; 

		if (x > 368 && x < 658 && y > (yt) && y < (yt+55)) {
			MusicalBottles.resetTime = this.getTime();  // start timer for the button being pressed
			MusicalBottles.Reporting.Reset();
			MusicalBottles.storeLanguageSetting();		
			;
		}
	},

	//
	// --- Handle Recording and Playback ---
	//
	
	/*
			initial		play	stop	record
	play	dis			dis		ok		dis		
	pause	dis			ok		dis		ok
	record	ok			dis		ok		dis
	stop	dis			ok		dis		ok
	*/
	date: null,
	StartRecording: function() {
		console.log("Starting Recording...");
		MusicalBottles.Recording = true;
		MusicalBottles.sb_recorded = true; // used to show a prompt
		MusicalBottles.RecordingItems = new Array();
		MusicalBottles.MusicalNotes = new Array();
		MusicalBottles.CurrentNoteIndex= 0;
		MusicalBottles.date = new Date();
		MusicalBottles.RecordingTime = MusicalBottles.date.getTime();
		MusicalBottles.Reporting.increment(cfg.topics[0].reports[MusicalBottles.REP_RECORDED].name, "Correct");
		
        for (var button in cfg.topics[0].buttons) {	
            var b = cfg.topics[0].buttons[button];
			if (b.name.indexOf("record") !== -1) {
				b.state = 'disabled';
			} else if (b.name.indexOf("stop") !== -1) {
				b.state = 'up';
			} else if (b.name.indexOf("pause") !== -1) {
				b.state = 'disabled';
			} else if (b.name.indexOf("play") !== -1) {
				b.state = 'disabled';
			}				
		}
	},
	
	StopRecording: function() {
		console.log("Stopping recording...");
		MusicalBottles.Recording = false;

        for (var button in cfg.topics[0].buttons) {	
            var b = cfg.topics[0].buttons[button];
			if (b.name.indexOf("record") !== -1) {
				b.state = 'up';
			} else if (b.name.indexOf("stop") !== -1) {
				b.state = 'disabled';
			} else if (b.name.indexOf("pause") !== -1) {
				b.state = 'disabled';	
			} else if ((b.name.indexOf("play") !== -1) && (MusicalBottles.RecordingItems.length > 0)) {
				b.state = 'up';
			}			
		}		
	},
	
	createSineWave: function(bottle) {
				MusicalBottles.xwlscale = bottle.wavelength_scale / 2
				MusicalBottles.ix = -(360/MusicalBottles.xwlscale);	
				cn = MusicalBottles.nHits; // MusicalBottles.CurrentNoteIndex;
				//console.log("num hits "+cn);
				MusicalBottles.wLength[cn] = Math.floor((360/MusicalBottles.xwlscale)); // gives weird effects if not an integer!
				MusicalBottles.sx[cn] = -MusicalBottles.wLength[cn];
				var i = 0;
				var mx = 60; // max y -height
				do {
					var rad = i * MusicalBottles.xwlscale * (2 * Math.PI)/ 360;
					MusicalBottles.outputs[cn][i] = -mx * Math.sin(rad);
					//console.log("values "+i+" "+CurrentNoteIndex+" "+MusicalBottles.outputs[MusicalBottles.nHits][i]+" "+MusicalBottles.wLength[MusicalBottles.nHits]);
					i++;
				} while (i < MusicalBottles.wLength[cn]);
				MusicalBottles.nHits++;			
	},
	
	PlaybackLoop: function(iter) {
	
		var time = MusicalBottles.RecordingItems[iter].Time;
		if (iter > 0) {
			time -= MusicalBottles.RecordingItems[iter - 1].Time;
		}
		MusicalBottles.RecordingItems[iter].Callback = setTimeout(function(iter){
				bottle = MusicalBottles.RecordingItems[iter].Bottle;
				MusicalBottles.playSound("audio/" + MusicalBottles.RecordingItems[iter].Bottle.sound+MusicalBottles.soundType, null, null, false);
				clearTimeout(MusicalBottles.RecordingItems[iter].Callback);
				MusicalBottles.RecordingItems[iter].Callback = 0;
				iter++;
				MusicalBottles.CurrentNoteIndex++;
				MusicalBottles.PlaybackIndex = MusicalBottles.CurrentNoteIndex;
				MusicalBottles.xdrumstick = bottle.rect.left + 25 + (20 * Math.random()); MusicalBottles.ydrumstick = bottle.rect.top + 70 + (30 * Math.random()); 
		
				if (options.showMusic == "sinewave")
					MusicalBottles.createSineWave(bottle);
				
				if (iter < MusicalBottles.RecordingItems.length){
					MusicalBottles.PlaybackLoop(iter);
				} else {
					MusicalBottles.StopPlayback();
					//MusicalBottles.CurrentNoteIndex = 0;
				}
		}, time, iter);
		
	},
	
	StartPlayback: function () {
		if (MusicalBottles.Recording) {
			MusicalBottles.StopRecording();
		}
		console.log("Starting playback...");
		MusicalBottles.Playback = true;	
		MusicalBottles.nHits = 0; // pjm test		
		MusicalBottles.CurrentNoteIndex= MusicalBottles.StartIndex;
		MusicalBottles.PlaybackIndex = MusicalBottles.CurrentNoteIndex;		
		MusicalBottles.PlaybackLoop(MusicalBottles.StartIndex);
		MusicalBottles.Reporting.increment(cfg.topics[0].reports[MusicalBottles.REP_PLAYED].name, "Correct");
		
        for (var button in cfg.topics[0].buttons) {	
            var b = cfg.topics[0].buttons[button];
			if (b.name.indexOf("record") !== -1) {
				b.state = 'disabled';
			} else if (b.name.indexOf("stop") !== -1) {
				b.state = 'up';
			} else if (b.name.indexOf("pause") !== -1) {
				b.state = 'up';
			} else if (b.name.indexOf("play") !== -1) {
				b.state = 'disabled';
			}				
		}		
	},
	
	StartIndex: 0,
	PausePlayback: function () {
		console.log("Pausing playback...");
		for (var item_index in MusicalBottles.RecordingItems) {
			if (MusicalBottles.RecordingItems[item_index].Callback !== 0) {
				MusicalBottles.StartIndex =item_index;
				break;
			}
		}
		for (var item_index in MusicalBottles.RecordingItems) {
			clearTimeout(MusicalBottles.RecordingItems[item_index].Callback);
			MusicalBottles.RecordingItems[item_index].Callback = 0;
		}

        for (var button in cfg.topics[0].buttons) {	
            var b = cfg.topics[0].buttons[button];
			if (b.name.indexOf("record") !== -1) {
				b.state = 'disabled';
			} else if (b.name.indexOf("stop") !== -1) {
				b.state = 'up';
			} else if (b.name.indexOf("pause") !== -1) {
				b.state = 'disabled';
			} else if (b.name.indexOf("play") !== -1) {
				b.state = 'up';
			}				
		}		
	},
	
	StopPlayback: function () {
		console.log("Stopping playback...");
		MusicalBottles.Playback = false;
		
		for (var item_index in MusicalBottles.RecordingItems) {
			clearTimeout(MusicalBottles.RecordingItems[item_index].Callback);
			MusicalBottles.RecordingItems[item_index].Callback = 0;
		}
		
		MusicalBottles.StartIndex = 0;
		
        for (var button in cfg.topics[0].buttons) {	
            var b = cfg.topics[0].buttons[button];
			if (b.name.indexOf("record") !== -1)  {
				b.state = 'up';
			} else if (b.name.indexOf("stop") !== -1) {
				b.state = 'disabled';
			} else if (b.name.indexOf("pause") !== -1) {
				b.state = 'disabled';
			} else if (b.name.indexOf("play") !== -1) {
				b.state = 'up';
			}				
		}		
	},
	

	//
	// --- Handle Adding and Removing Bottles ---
	//
    leftPosition: function(nBottles, i) {		
		var left = [420, 0, 0, 0, 0, 		351,490, 0, 0, 0, 	282, 421, 559, 0, 0, 	214, 352, 490, 628, 0, 		165, 293, 421, 549, 677];
		var offs = 5 * (nBottles - 1) + i;
		return left[offs];
    },
	
	disablePlayButton: function(nBottles, i) {
		
		// disable the play button. done after add or remove (not fill or empty)
		//console.log("try to disable play");
		for (var button in cfg.topics[0].buttons) {		
            var b = cfg.topics[0].buttons[button];	
			if (b.name == "play") { 
				b.state = 'disabled';
				//console.log("play disabled");
 	        }
 	   }
	},
	
    AddBottle: function() {
		// 20.12.14 Code not finished. Tried to introduce a second topic so that all first topic buttons displayed one per activity
		// and second topic, one per bottle, but ran out of time (reading images for second buttons).
        var nBottles = MusicalBottles.Bottles.length;
        if (nBottles >= MusicalBottles.MaxBottles){
            return;   
        }
        var bottle;
		var SecondTopic = 1; //MusicalBottles.CurrentTopic;
        // make local copy, just in case
        var items = cfg.topics[MusicalBottles.CurrentTopic].items;
        for (var it in items) {
            if (items[it].name === "bottle0") {
                bottle = application.extend(items[it]);
            }
        }
        
        var button;
        for (var it in cfg.topics[SecondTopic].buttons) {
			//console.log("adding "+it+" "+cfg.topics[SecondTopic].buttons[it].name);
            if (cfg.topics[SecondTopic].buttons[it].name === "empty_bottle" ) {
                button = application.extend(cfg.topics[MusicalBottles.CurrentTopic].buttons[it]); // inherits properties from topic 0, ie down.width
				console.log("button found "+button.down.width);
                break;
            }
        }
        bottle.button = button;
        if (bottle.name === "bottle0") {
            bottle.button.state = 'disabled';
        } else {
            bottle.button.state = 'up';   
        }
		bottle.button.state = 'disabled';
        bottle.interval = null;
        MusicalBottles.Bottles.push(bottle);
        
        nBottles = MusicalBottles.Bottles.length;
		console.log("Added bottle");
        for (var it = 0; it < nBottles; it++) {
            var bottle = MusicalBottles.Bottles[it];
            //var left, top;
            //var offset = (it+0.5) - (nBottles/2);
            var left = MusicalBottles.leftPosition(nBottles, it); //(768/2) + offset*bottle.image.width*(1.5) + bottle.image.width/2.5;
            //top = 240;
            var rect = {
                left: 0,
                top: 0,
                width: 0,
                height: 0,
            };
            rect.left = left;
            rect.top = this.bottleTop;
            rect.width = bottle.image.width;
            rect.height = bottle.image.height;
            MusicalBottles.Bottles[it].rect = rect;
			console.log("# "+it+" "+left);
			
			// this seems to be where the 'empty' icon is set up - not sure how 'extend' works
            var buttonRect = application.extend(rect);
            buttonRect.width = button.down.width;
            buttonRect.height = button.down.height;
            buttonRect.top += rect.height + 3;
            buttonRect.left = rect.left - buttonRect.width/2 + rect.width/2;
            bottle.button.rect = buttonRect;
        }
        
		MusicalBottles.disablePlayButton();
        
    },
    
    RemoveBottle: function() {
         if (MusicalBottles.Bottles.length <= 1){
            return;   
        }
        // Remove last added bottle
        MusicalBottles.Bottles.splice(MusicalBottles.Bottles.length - 1, 1);
		console.log("Removed bottle");
        
        var nBottles = MusicalBottles.Bottles.length;
        for (var it = 0; it < nBottles; it++) {
            var bottle = MusicalBottles.Bottles[it];
            //var left, top;
            //var offset = (it+0.5) - (nBottles/2);
            var left = MusicalBottles.leftPosition(nBottles, it); //(768/2) + offset*bottle.image.width*(1.5) + bottle.image.width/2.5;
            //top = 350;
            var rect = {
                left: 0,
                top: 0,
                width: 0,
                height: 0,
            };
            rect.left = left;
            rect.top = this.bottleTop;
            rect.width = bottle.image.width;
            rect.height = bottle.image.height;
            MusicalBottles.Bottles[it].rect = rect;
            var buttonRect = application.extend(bottle.button.rect);
            buttonRect.left = rect.left - buttonRect.width/2 + rect.width/2;
            bottle.button.rect = buttonRect;
			console.log("# "+it+" "+left);			
        }
		// disable the play button
		MusicalBottles.disablePlayButton();
		
		/*console.log("try to disable play");
		for (var button in cfg.topics[0].buttons) {		
            var b = cfg.topics[0].buttons[button];	
			if (b.name == "play") { 
				b.state = 'disabled';
				console.log("play disabled");
 	        }
 	   }*/
        //MusicalBottles.UpdateButtons();

    },
    
    EmptyBottle: function(bottle) {
		            console.log("Emptying bottle");
        if (bottle.interval === null) {
            bottle.interval = setInterval(function(){MusicalBottles.EmptyBottleInterval(bottle)}, 200); //0);
            bottle.playingSound = MusicalBottles.playAudio("audio/glug"+MusicalBottles.soundType);
			//bottle.playingSound = MusicalBottles.playAudio("audio/glug_short"+MusicalBottles.soundType);

			MusicalBottles.emptying = true;
			MusicalBottles.sb_emptied = true; // used to show a prompt
        }
    },
	
	
	//
	// --- Handle Filling and Emptying bottles ---
	//
	
    
    FillBottle: function(bottle) {
        if (bottle.interval === null) {
            var level = parseInt(bottle.name.split("bottle")[1]);
            console.log("Begin filling bottle");
            MusicalBottles.oneshotPourSound = true;
            //MusicalBottles.Jug.sound = MusicalBottles.playAudio("audio/pour"+MusicalBottles.soundType);
            MusicalBottles.jugOffset = 1;
            MusicalBottles.Jug.state = 'pouring';
            clearInterval(MusicalBottles.Jug.interval);
            MusicalBottles.Jug.interval = null;
            MusicalBottles.Jug.interval = setInterval(function() {MusicalBottles.AnimateJug(bottle);}, 200);
			bottleFilling = bottle;
            bottle.interval = setInterval(function() {MusicalBottles.FillBottleInterval(bottle);}, 400); // 1000
        }
    
    },
    
    StopFillingBottle: function(bottle) {
		MusicalBottles.Jug.sound.pause();
        if (bottle.name === "bottle0") {
            bottle.button.state = 'disabled';
        } else {
            bottle.button.state = 'up';   
        }   
        
        MusicalBottles.jugOffset = -1;
		MusicalBottles.Jug.state = 'held'; // when held to pour 'none';
		console.log("stop pouring");
        clearInterval(bottle.interval);
        bottle.interval = null;
		bottleFilling = null;
    },
        
    FillBottleInterval: function(linkedBottle) {
        var bottle = linkedBottle;
        var level = parseInt(bottle.name.split("bottle")[1]);
        if (level === 12) {
            // Bottle full
            MusicalBottles.StopFillingBottle(bottle);
            console.log("Bottle full "+bottle.interval);
        } else {
			if (MusicalBottles.Jug.anim_index >= 4) { // only increment the level when the jug is pouring properly
              level = level + 1;
			  bottle.playingSound = MusicalBottles.playSound("audio/pour2b"+MusicalBottles.soundType);
			  MusicalBottles.sb_filled = true; // used to show a prompt
			}
            for (var it in cfg.topics[MusicalBottles.CurrentTopic].items) {
                var newBottle = application.extend(cfg.topics[MusicalBottles.CurrentTopic].items[it]);
                var newBottleLevel = newBottle.name.split("bottle")[1];
                if (newBottleLevel == level) {
					if (level > 1) // slight delay otherwise graphics are updated too soon
					  bottle.button.state = 'up';
                    bottle.name = newBottle.name;
                    bottle.image = newBottle.image;
                    bottle.file = newBottle.file;
                    bottle.sound = newBottle.sound;
					bottle.note_height = newBottle.note_height;
					bottle.wavelength_scale = newBottle.wavelength_scale;
                    break;
                }                 
            }
        }
        //console.log("Filling bottle - " + level+" "+bottle.state);
    },
    
    EmptyBottleInterval: function(bottle) {
        var level = parseInt(bottle.name.split("bottle")[1]);
        console.log( "Bottle level: " + level);
        if ((level == 0) || !MusicalBottles.emptying) {
            MusicalBottles.StopFillingBottle(bottle);
            bottle.playingSound.pause();
			MusicalBottles.emptying = false;
			for (var button in cfg.topics[0].buttons) {	
				var b = cfg.topics[0].buttons[button];
				if (b.name.indexOf("empty") !== -1) {
					if (level == 0) {
					  b.state = 'disabled';
					  console.log( "empty button is disabled " + level);
				    }
				}
			}			
        } else {
            level = level - 1;
			//bottle.playingSound = MusicalBottles.playSound("audio/glug_short"+MusicalBottles.soundType);
            for (var it in cfg.topics[MusicalBottles.CurrentTopic].items) {
                var newBottle = application.extend(cfg.topics[MusicalBottles.CurrentTopic].items[it]);
                var newBottleLevel = newBottle.name.split("bottle")[1];
                if (newBottleLevel == level) {
                    bottle.name = newBottle.name;
                    bottle.image = newBottle.image;
                    bottle.file = newBottle.file;
                    bottle.sound = newBottle.sound;
					bottle.note_height = newBottle.note_height;
					bottle.wavelength_scale = newBottle.wavelength_scale;
                    break;
                }                 
            }
        }
    },
        
        jugOffset: 1,
    AnimateJug: function(bottle) {
        if (MusicalBottles.Jug.anim_index === 'undefined') {
             MusicalBottles.Jug.anim_index = 0;  
        }
		//console.log("AnimateJug: "+MusicalBottles.Jug.anim_index +" "+ MusicalBottles.jugOffset);
        MusicalBottles.Jug.anim_index += MusicalBottles.jugOffset;
  
		if (MusicalBottles.notOverAnyBottle) { // this is an emergency stop for pouring
			MusicalBottles.Jug.anim_index = 0;
		} 
		
        if (MusicalBottles.Jug.anim_index <= 0) {
            MusicalBottles.Jug.anim_index = 0;
            MusicalBottles.Jug.image = MusicalBottles.Jug.anim[0];
            clearInterval(MusicalBottles.Jug.interval);
            MusicalBottles.Jug.interval = null;
            //MusicalBottles.Jug.sound.pause();
        }
        if (MusicalBottles.jugOffset === -1) {
            MusicalBottles.StopFillingBottle(bottle);
        }
        if (MusicalBottles.Jug.anim_index >= MusicalBottles.Jug.anim.length - 1) {
            MusicalBottles.Jug.anim_index = MusicalBottles.Jug.anim.length - 1; 		
        }
		if ((MusicalBottles.Jug.anim_index == 1) && MusicalBottles.oneshotPourSound) {
			MusicalBottles.Jug.sound = MusicalBottles.playAudio("audio/pour"+MusicalBottles.soundType);
			MusicalBottles.oneshotPourSound = false;
		}
        MusicalBottles.Jug.image = MusicalBottles.Jug.anim[MusicalBottles.Jug.anim_index];
        if (MusicalBottles.jugOffset > 0) {
            // 1110 MusicalBottles.Jug.rect.left = bottle.rect.left + bottle.rect.width/2 - 15;
            // 1110 MusicalBottles.Jug.rect.top = bottle.rect.top - 168;
        }
		            
        //console.log("Anim Jug "+MusicalBottles.Jug.anim_index+" "+MusicalBottles.Jug.rect.top);
    },
	
	// handles mouse clicks when in an activity
	gameClicked: function (x, y) {
		//MusicalBottles.ix = 600;
        MusicalBottles.renderGame();
        //console.log("in Game clicked: " + x + ", " + y);
		if (this.hand) {
			return;
		}
		// *** test code for storing report data
		//var offs = 1; // cfg.topics[0].reports[MusicalBottles.REP_TUNES].name
		//MusicalBottles.Reporting.increment(cfg.topics[0].reports[offs].name, "Correct");	
		//MusicalBottles.Reporting.increment(cfg.topics[0].reports[MusicalBottles.REP_RECORDED].name, "Correct");
		//MusicalBottles.storeLanguageSetting();
		// *** end of test code
		
        // Bottom bar buttons
		var currentTime = this.getTime();
		if (y>(this.CanvasHeight-MusicalBottles.adMobHeightAdjust)-58 && y<(this.CanvasHeight-MusicalBottles.adMobHeightAdjust) ) {
			/*if (x>6 && x<6+78) { // back button clicked
				if (this.toasterTimer!==null) {
				  clearTimeout(this.toasterTimer);
				}
				this.Location = "home";
                var str = 'auto';
                document.getElementById("gamecanvas").style.cursor = str;
				this.stopAllAudio();
				this.doRender();
			}*/
			
			/*if (x>425 && x<425+138) { // play help audio
				this.playAudio(cfg.topics[this.CurrentTopic].help);
			}*/
        }
		if(x >= this.icnAudio.x && x < (this.icnAudio.x+this.icnAudio.w) && y >= this.icnAudio.y && y < (this.icnAudio.y+this.icnAudio.h)) {
			//if (y>724 && y<768 && x>947 && x<1012) { // help audio clicked
				this.playAudio("help_bottles"+this.soundType);
				//  MusicalBottles.playAudio("bottles"+MusicalBottles.soundType);  
				this.helpAudioPlaying = true;
		}			
        //console.log("Game clicked: " + x + ", " + y);	
		var outs = "pd ";		
        // Check topic buttons
        for (var button in cfg.topics[0].buttons) {	
            var button = cfg.topics[0].buttons[button];	
			//console.log("button1769 "+button.name);			
        //for (var button_index in MusicalBottles.Buttons) {
        //    var button = MusicalBottles.Buttons[button_index];
            // Player should have no knowledge of these, don't bother checking for press
            if (button.state === 'hidden') {
                continue;   
            }
	
            if ((x > button.rect.left && x < button.rect.left + button.rect.width) && (y > button.rect.top && y < button.rect.top + button.rect.height)) {
                // Player can see they can't press this, make noises at them just in cas they dont understand
             console.log("button2144 "+button.name+" "+button_index+" "+button.state);
			 if (button.state === 'disabled') {
					MusicalBottles.playSound("audio/softerror.mp3" /*+ MusicalBottles.soundType*/, null, null, false);
                    continue;   
             } else if (button.state === 'down') {
                    // button was down previous frame - probably never hit this scenario, but included for completeness
                    continue;
             } else {
				 	console.log("button2152 "+button.name+" "+button.rect.left+" "+button.rect.top);
                    // Button is up, now clicked, so down - this is set in event handlers - just handle game happenings for button press here
                    if (button.name.indexOf("add_bottle") !== -1) {
                        MusicalBottles.AddBottle();
						button.downTime = this.getTime(); // start timer for the button being pressed
						outs = outs+ " "+button+" "+(MusicalBottles.buttonPressedTime[button]); //+buttonData.name;
						if (MusicalBottles.Bottles.length > 4){					
							button.state = 'disabled'
						} else
							button.state = 'down';
						for (var button_index in cfg.topics[0].buttons) {	
							var removeButton = cfg.topics[0].buttons[button_index];	
							  if (removeButton.name.indexOf("remove_bottle") !== -1) {
								removeButton.state = 'up';
							}
						}
						break;
                    } else if (button.name.indexOf("remove_bottle") !== -1) {

                        MusicalBottles.RemoveBottle();   
						button.downTime = this.getTime();  // start timer for the button being pressed
						outs = outs + "rb "+button.downTime;
						if (MusicalBottles.Bottles.length <= 1){
							//	console.log("remove button disabled");						
							button.state = 'disabled'
						} else
							button.state = 'down';
						for (var button_index in cfg.topics[0].buttons) {	
							var removeButton = cfg.topics[0].buttons[button_index];	
							  if (removeButton.name.indexOf("add_bottle") !== -1) {
								removeButton.state = 'up';
							}
						}						
						break;
                    } else if (button.name.indexOf("empty_bottle") !== -1) {
						console.log("empty 1819");
                        MusicalBottles.EmptyBottle(button.linkedBottle); 
						break;						
                    } else if (button.name.indexOf("record") !== -1) {
						button.downTime = this.getTime();  // start timer for the button being pressed
						button.state = 'down';
						break;
					} else if (button.name.indexOf("back") !== -1) {
						button.state = 'down';
						button.downTime = this.getTime();  // start timer for the button being pressed
								// 13.08.16 bottleFilling sb MusicalBottles.bottleFilling surely? but for now, i'm ignoring the error
								if (bottleFilling != null) {
								  MusicalBottles.StopFillingBottle(bottleFilling);
								}
						break;
					} else if (button.name.indexOf("stop") !== -1) {
						button.state = 'down';
						button.downTime = this.getTime();  // start timer for the button being pressed
						break;
					} else if (button.name.indexOf("play") !== -1) {
						button.state = 'down';
						button.downTime = this.getTime();  // start timer for the button being pressed
						break;
					} else if (button.name.indexOf("pause") !== -1) {
						button.downTime = this.getTime();  // start timer for the button being pressed
						button.state = 'down';
						break;
					}
					outs = outs+ " "+(MusicalBottles.buttonPressedTime[button]); //+buttonData.name;
                }
                //console.log("Button " + button.state + " when game clicked");
            }
        }
		//console.log("outs is "+outs);
		// play a note if mouse clicked on bottle
        for (var bottle_index in MusicalBottles.Bottles) {
            var bottle = MusicalBottles.Bottles[bottle_index];
			//console.log("hit "+bottle_index);
            if ((x > bottle.rect.left && x < bottle.rect.left + bottle.rect.width) && (y > bottle.rect.top && y < bottle.rect.top + bottle.rect.height) &&
			(MusicalBottles.Jug.state != 'pouring')) {
				//console.log("in loop "+MusicalBottles.Recording);
			if (MusicalBottles.Recording) {
					var item = application.extend(MusicalBottles.RecordingItem);
					item.Bottle = application.extend(bottle);
					MusicalBottles.date = new Date();
					//console.log("Time: " + MusicalBottles.date.getTime());
					item.Time = MusicalBottles.date.getTime() - MusicalBottles.RecordingTime;
					MusicalBottles.RecordingItems.push(item);
				}
                MusicalBottles.playSound("audio/" + bottle.sound + MusicalBottles.soundType, null, null, false);
				MusicalBottles.sb_clicked = true; // used to show a prompt
				MusicalBottles.Reporting.increment(cfg.topics[0].reports[MusicalBottles.REP_NOTES].name, "Correct");
				if (options.showMusic == "sinewave")
					MusicalBottles.createSineWave(bottle);

                //console.log (bottle.name + " was clicked "+MusicalBottles.wLength[MusicalBottles.nHits]+" "+MusicalBottles.xwlscale);
				var note = {
							x: MusicalBottles.lastNoteX,
							y: bottle.note_height,
							}
				if (MusicalBottles.MusicalNotes.push(note) > 9) {
					MusicalBottles.CurrentNoteIndex++;
				} else {
					MusicalBottles.lastNoteX += MusicalBottles.noteOffset
				}
				
                MusicalBottles.Smudge.rect.left = bottle.rect.left - MusicalBottles.Smudge.rect.width/2 + bottle.rect.width/2;
				
				// 1109 clearTimeout(MusicalBottles.smudgeCallback);
				// 1109 clearInterval(MusicalBottles.Smudge.interval);
				
				MusicalBottles.Smudge.rect.top = MusicalBottles.Smudge.maxTop;
				
				MusicalBottles.smudgeDirection = 1;	
                MusicalBottles.Smudge.interval = setInterval(function(){MusicalBottles.SlideSmudge();}, 50);
				MusicalBottles.smudgeCallback = setTimeout(function() {
					MusicalBottles.smudgeDirection = 0;
					MusicalBottles.smudgeCallback = setTimeout(function() {MusicalBottles.smudgeDirection = -1;}, MusicalBottles.Smudge.upTime);
				},	400);
				
						
            }
            
			// handle the empty button - this is one per bottle, so handled differently to the other buttons
            var button = bottle.button;
            if ((x > button.rect.left && x < button.rect.left + button.rect.width) && (y > button.rect.top && y < button.rect.top + button.rect.height)) {
                // Player can see they can't press this, make noises at them just in cas they dont understand
                if (button.state === 'disabled') {
                    // TODO: Play audio
                    continue;   
                } else if (button.state === 'down') {
                    // button was down previous frame - probably never hit this scenario, but included for completeness
                    continue;
                } else {
                    // Button is up, now clicked, so down - this is set in event handlers - just handle game happenings for button press here
					console.log("empty 2276 plus "+MusicalBottles.emptying);
					if (!MusicalBottles.emptying)
                       MusicalBottles.EmptyBottle(bottle);   
                    else {
					   MusicalBottles.emptying = false;
					   MusicalBottles.FillBottleInterval(bottle);
					}
                }
                //console.log("Button " + button.state + " when game clicked");
            }
                
        }
	},

	// handles the mouse clicked when in the game (part 2)
	gameClicked2: function(event) {
		if (MusicalBottles.Location !== "game") {
			return;
		}

		var ctx = this.getCanvasContext();
		var x = 0;
		var y = 0;

		MusicalBottles.mousePosition(event, false); x = MusicalBottles.xpsn; y = MusicalBottles.ypsn;	
		
		var outs = "Md "+x + ", " + y;
		//console.log("Mouse down "+x + ", " + y);	
        
        for (var button_index in MusicalBottles.Buttons) {
            var button = MusicalBottles.Buttons[button_index];
            // Player should have no knowledge of these, don't bother checking for press
            if (button.state === 'hidden' || button.state === 'disabled') {
                continue;   
            }
            
            if ((x > button.rect.left && x < button.rect.left + button.rect.width) && (y > button.rect.top && y < button.rect.top + button.rect.height)) {                
                button.state = 'down';     
                //outs = outs + " "+ button_index+" "+button.name +" " + button.state;
            }
			//outs = outs + button.state;
            //console.log(button.name + "'s state is " + button.state);
        }
		outs = outs + " "+MusicalBottles.Buttons[0].name +" " + MusicalBottles.Buttons[0].state;
		//console.log(outs);
        
        for (var bottle_index in MusicalBottles.Bottles) {
            var button = MusicalBottles.Bottles[bottle_index].button; 
            if (button.state === 'hidden' || button.state === 'disabled') {
                continue;   
            }
            if ((x > button.rect.left && x < button.rect.left + button.rect.width) && (y > button.rect.top && y < button.rect.top + button.rect.height)) {                
                button.state = 'down';     
                
            }
        }
		
		if ((x > MusicalBottles.Jug.rect.left && x < MusicalBottles.Jug.rect.left + MusicalBottles.Jug.rect.width) && (y > MusicalBottles.Jug.rect.top && y < MusicalBottles.Jug.rect.top + MusicalBottles.Jug.rect.height)) {       
                if (MusicalBottles.Jug.state === 'pouring') {
                    MusicalBottles.jugOffset = -1;
                      
                }
                MusicalBottles.Jug.state = 'held';
                MusicalBottles.hand = MusicalBottles.Jug;
                //MusicalBottles.playAudio(MusicalBottles.hand.audiofile);
                MusicalBottles.handOffsetX = x - MusicalBottles.Jug.rect.left;
                MusicalBottles.handOffsetY = y - MusicalBottles.Jug.rect.top;
         }
		
		;
	},
	

 	
	//
	// --- UTILITY Functions ---
	//
		
	// load data from the browser's memory. 
	// Not compatible with IE/Edge, hence the try/catch. 
	loadLanguageSetting: function() {

	  try {
		var locale = window.localStorage.getItem("MusicalBottlesLocale");
		if (locale === null) {
			LanguageStrings.setLocale();
			Sorting.storeLanguageSetting();
		} else {
			LanguageStrings.setLocale(locale);
			console.log("locale is "+locale);
		}
		
		var showMusic = window.localStorage.getItem("MusicalBottlesShow"); // Musical notes or sound waves
		if (showMusic != null) {
			options.showMusic = showMusic;
			//console.log("currency is "+currency);
		}

		// It stores as a string, therefore need to set boolean fron the string
		var showPrompt = window.localStorage.getItem("MusicalBottlesPrompt"); // Musical notes or sound waves
		if (showPrompt != null) {
			if (showPrompt == 'true')
				options.showPrompt = true;
			else
				options.showPrompt = false;
			console.log("prompt is "+options.showPrompt+" "+showPrompt);
		}
		
		var sound = window.localStorage.getItem("MusicalBottlesSound");
		if (sound === null) {
			options.soundEnabled = true;
			Sorting.storeLanguageSetting();			
		} else
			// i think this is wrong - we need to do as showPrompt
			options.soundEnabled = sound;	
			
		var reportData = window.localStorage.getItem("MusicalBottlesReportData");
		if (reportData === null) {
			// do nothing for now
			//console.log("loadlanguage settings, rep data null");
		} else {
			try {
			    // we don't read report data (home users don't care; schools swap devices) better as an option!
				this.Reporting.setAllData(JSON.parse(reportData));	
				console.log("loadlanguage settings, rep data "+reportData);
			} catch (error) {
				// storage might be exceeded: ignore
			    console.log("loadlanguage settings, rep data error "+reportData);
			}			
		}
	  } catch (error) {
				// This may be the fact that storage does not work with IE
			    console.log("loadlanguageSettings, error: "+error);
	  }		
	},
	
	//load and save data to the browser's memory. Not compatible with IE/Edge, hence the try/catch. 
	storeLanguageSetting: function() {
		try {
            window.localStorage.removeItem("MusicalBottlesLocale");
            window.localStorage.setItem("MusicalBottlesLocale", LanguageStrings.CurrentLocale);
            window.localStorage.removeItem("MusicalBottlesShow");
            window.localStorage.setItem("MusicalBottlesShow", options.showMusic);
            window.localStorage.removeItem("MusicalBottlesPrompt");
            window.localStorage.setItem("MusicalBottlesPrompt", options.showPrompt);
			console.log("store prompt is "+options.showPrompt);			
            window.localStorage.removeItem("MusicalBottlesSound");
            window.localStorage.setItem("MusicalBottlesSound", options.soundEnabled);	
			
            window.localStorage.removeItem("MusicalBottlesReportData");				
            window.localStorage.setItem("MusicalBottlesReportData", JSON.stringify(this.Reporting.getAllData()));	
			//console.log("storelangiage stettings2 "+JSON.stringify(this.Reporting.getAllData()));		
		} catch (error) {
				// This may be the fact that storage does not work with IE
			    console.log("storelanguageSettings, error: "+error);
        }
		//console.log("storelanguagesetting, level = "+options.difficulty);
	},
	
	// returns the time without needing to declare date variable first
	getTime: function() {
		
		var date = new Date();
		return date.getTime();
	},	

	//
	// --- SOUND Functions ---
	//
	
	soundType : ".ogg",
	isIE : false,
	isIpad: false, // this is used elsewhere
	isMacintosh: false, // used to change prompts from tap to click
	isAndroid: false,   // android gets x and y values from touch[0], not event
	//rewardsound: 0,
	//rewardsounds: ["correct.mp3","welldone.mp3"],
	
	// stop all sound from playing, ie when another sound is played, or user leaves the page 
	stopAllAudio: function() {
		var sounds = document.getElementsByTagName('audio');
		for (var i = 0; i < sounds.length; i++) {
			sounds[i].pause();
            if(sounds[i].parentNode.contains(sounds[i])) {
            sounds[i].parentNode.removeChild(sounds[i]);
            }
		}
	},

	// look at the browser type and set the appropriate sound format
	// .wav for ??? (not compressed for online use)
	// .ogg for ??? (preferred)
	// .mp3 for ??? (nice, but vaguely potential copyright issue)
	setSoundFormat: function() {
		 var ua = navigator.userAgent.toLowerCase();

		 var check = function(r) {
			   return r.test(ua);
		 };
		 
		 var DOC = document;
		 var isStrict = DOC.compatMode == "CSS1Compat";
		 var isOpera = check(/opera/);
		 var isChrome = check(/chrome/);
		 var isWebKit = check(/webkit/);
		 var isEdge = check(/edge/);
		 var isSafari = !isChrome && check(/safari/);
		 var isSafari2 = isSafari && check(/applewebkit\/4/); // unique to Safari 2
		 var isSafari3 = isSafari && check(/version\/3/);
		 var isSafari4 = isSafari && check(/version\/4/);
		 isMacintosh = isSafari && check(/macintosh/);
		 this.isAndroid = false;
		 if(ua.match( /Android/i ) )
			this.isAndroid = true;		 
		 MusicalBottles.isIE = !isOpera && check(/msie/);
		 if (!(window.ActiveXObject) && "ActiveXObject" in window)
			MusicalBottles.isIE = true; // test for IE 11 and 12
		// if (Object.hasOwnProperty.call(window, "ActiveXObject") && !window.ActiveXObject)
		//	MusicalBottles.isIE = true;
		//console.log(this.isIE+" "+(window.ActiveXObject)+" "+window+" "+("ActiveXObject" in window));
		//console.log(navigator.appName);
		 MusicalBottles.browserName = ua;
		 MusicalBottles.soundType = ".ogg";
		 console.log("browser is "+ua);
		 console.log("is safari "+isSafari+" is edge "+isEdge);

		 if (MusicalBottles.isIE || isEdge){
			   MusicalBottles.soundType = ".mp3";
			   console.log("browser IE detected");
		 }

		 if (isSafari) {
			   MusicalBottles.soundType = ".mp3";
			   console.log("browser Safari detected");
		 }
		 console.log("sound type set to "+MusicalBottles.soundType);
		 MusicalBottles.soundType = ".wav";
		 MusicalBottles.browserName = MusicalBottles.soundType; //ua;
	},
	
	// For voice audio, adds the locale to the filename before calling playSound
	// this does not need a file extenstion 
	playAudio: function(src) {
			console.log("playaudio "+src);
		if (options.soundEnabled) {
		  this.stopAllAudio();
		  src = "audio/" + LanguageStrings.CurrentLocale + "/" + src;
		  return this.playSound2(src/*+MusicalBottles.soundType*/, null, null, false);
		}
	},
	
		playSound: function(src){
	
		if (options.soundEnabled === true) {
				this.playSound2(src/*+MusicalBottles.soundType*/, null, null, false);
				/*var audio = document.createElement("audio");
	            audio.src = "audio/"+src;	
	            //audio.src = "audio/" + LanguageStrings.CurrentLocale + "/" + src;	
				console.log("hellox audio is "+src);				
	            audio.addEventListener("ended", function () {
	                this.parentNode.removeChild(this);
	            });
	            document.body.appendChild(audio);
	            audio.load();
				//audio.loop = true; this will repeat, but not in Firefox apparantly
	            audio.play();*/
		}
	},
	// plays an audio file
	// note ipad may use an ipad specific version
	playSound2: function (src, secondSrc, endOfSoundCallback, repeat) {
		
		console.log("playsoundy "+src);
        if (typeof device !== "undefined") {
            var sound = new Media(src, endOfSoundCallback);
            if (! repeat) {
                sound.play();
            } else {
                sound.play({numberOfLoops:-1});
            }
            if (secondSrc !== null) {
                setTimeout(function () {
                    new Media(secondSrc, endOfSoundCallback).play();
                }, 600);
            }
            return sound;
        } else {
            var audio = document.createElement("audio");
            audio.src = src;
			MusicalBottles.soundPlaying = 1;
			//console.log("sound started");
            if (repeat) {
                audio.loop = "loop";
            }
            audio.addEventListener("ended", function () {
                if (this.parentNode.contains(this)) {
					MusicalBottles.soundPlaying = 2; // needs resetting
					//console.log("sound ended"+this.soundPlaying);

                    this.parentNode.removeChild(this);
                }
                if (endOfSoundCallback !== null) {
                    endOfSoundCallback.call(this);
                }
            });
            document.body.appendChild(audio);
            audio.load();
            audio.play();

            if (secondSrc !== null) {
                var audio2 = document.createElement("audio");
                audio2.src = secondSrc;
                audio2.addEventListener("ended", function () {
                    if (this.parentNode.contains(this)) {
                        this.parentNode.removeChild(this);
                    }
                    if (endOfSoundCallback !== null) {
                        endOfSoundCallback.call(this);
                    }
                });
                document.body.appendChild(audio2);
                audio2.load();
                audio2.play();
            }
            return audio;
        }
    },
};
