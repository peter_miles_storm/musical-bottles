/**
 * Object ImagePreloader
 * An object to load an array of images and call a function when all loading is completed.
 * @param images
 * @param callback
 * @returns {ImagePreloader}
 */
function ImagePreloader (images, callback) {
	// store the call-back
	this.callback = callback;

	// initialize internal state.
	this.nLoaded = 0;
	this.nProcessed = 0;
	this.aImages = new Array;

	// record the number of images.
	this.nImages = images.length;

    this.onload = function() {
    	//document.write("image loaded<br>");
        this.bLoaded = true;
        this.oImagePreloader.nLoaded++;
        this.oImagePreloader.onComplete();
    };

    //The call-back function is stored for later use, then each image URL is passed into the preload() method.

    this.preload = function(image) {
    	//document.write("preload "+image+"<br>");
        // create new Image object and add to array
        var oImage = new Image;
        this.aImages.push(oImage);

        // set up event handlers for the Image object
        oImage.onload = this.onload;
        oImage.onerror = this.onerror;
        oImage.onabort = this.onabort;

        // assign pointer back to this.
        oImage.oImagePreloader = this;
        oImage.bLoaded = false;

        // assign the .src property of the Image object
        oImage.src = image;
    };

	// for each image, call preload()
	for (var i = 0; i < images.length; i++) {
		this.preload(images[i]);
	}

    this.onComplete = function() {
        this.nProcessed++;
        if (this.nProcessed == this.nImages) {
            this.callback(this.aImages, this.nLoaded);
        }
    };


    this.onerror = function() {
        this.bError = true;
        this.oImagePreloader.onComplete();
    };

    this.onabort = function() {
        this.bAbort = true;
        this.oImagePreloader.onComplete();
    };

	/**
	 * Returns a given image
	 * @param index
	 * @returns {Image} The image stored at the given index.
	 */
    
    this.getImage = function(index) {
    	return this.aImages[index];
    };

	
	
	this.getLength = function() {
		return this.aImages.length;
	};
}
