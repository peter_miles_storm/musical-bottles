﻿var Mag2DImages = {
    percloaded: 0, 
    numloaded: 0, 
    numtoload: 0,

    startLoadingImages: function (imagelistobject, callback, progress) {
        Mag2DImages.percloaded = 0;
        Mag2DImages.numloaded = 0;
        Mag2DImages.numtoload = 0;

        var checkLoading = function () {
            var nloaded = 0;

            for (var co in imagelistobject) {

                if (imagelistobject[co].img.complete && imagelistobject[co].img.naturalWidth !== undefined) {
                    nloaded++;
                }
            }

            Mag2DImages.numloaded = nloaded;
            Mag2DImages.percloaded = ((Mag2DImages.numloaded / Mag2DImages.numtoload) * 100) >>> 0;

            // update progress if a callback has been set
            if (progress!==undefined) {
                progress(Mag2DImages.percloaded);
            }

            if (nloaded === Mag2DImages.numtoload) {
                // all loaded
                callback();
            } else {
                setTimeout(checkLoading, 250);
            }
        }

        // get number of images to load
        var i;
        for (i in imagelistobject) {
            Mag2DImages.numtoload++;
        }

        // start the images loading
        var o;
        for (i in imagelistobject) {
            o = imagelistobject[i];
            o.img = new Image;
            o.img.src = o.src;  
        }

        setTimeout(checkLoading, 250); 
   },

    loadSingleImage: function (src, callback) {
        var img = new Image;

        var checkloaded = function () {
            if (!img.complete && img.naturalWidth === undefined) {
                setTimeout(checkloaded, 250);
            } else {
                callback(img);
            }
        }

        img.onload = function () {
            checkloaded();
        };
        img.src = src;
    }
}
