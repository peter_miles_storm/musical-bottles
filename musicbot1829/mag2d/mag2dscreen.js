﻿Mag2DScreen = {
    OriginalDimension: { width: 0, height: 0 }, // dimension of design i.e. HD 1920x1080
    ScreenDimension: { width: 0, height: 0 }, // dimension of canvas when scaled and fitted to screen

    AdHeight: 90,
    ScreenContext: null,
    ScreenCanvas: null,
    ParentDiv: null,
	// this.fontSize is in the size defined by the external class
	// and isn't the size sent to ctx
	fontSize: 24,

    ScreenLocation: { left: 0, top: 0 },

    setOriginalSize: function (dimension) {
        this.OriginalDimension = dimension;
    },

    calculateScreenDimension: function (adheight) {
        this.AdHeight = adheight;

        // Calculate the canvas width and height
        var rx =  window.innerWidth / this.OriginalDimension.width;
        var ry = (window.innerHeight - adheight) / this.OriginalDimension.height;
        var ratio = 1;

        if (rx < ry) ratio = rx; else ratio = ry;

        this.ScreenDimension.width = (ratio * this.OriginalDimension.width) >>> 0;
        this.ScreenDimension.height = (ratio * this.OriginalDimension.height) >>> 0;
		console.log("calcsa "+window.innerWidth+" "+window.innerHeight);
		console.log("calcsb "+this.OriginalDimension.width+" "+this.OriginalDimension.height+" "+ratio);
		console.log("calcsc "+this.ScreenDimension.width+" "+this.ScreenDimension.height);		
    },

    resize: function(renderfunction) {
        this.calculateScreenDimension(this.AdHeight);

        this.ScreenCanvas.width = this.ScreenDimension.width;
        this.ScreenCanvas.height = this.ScreenDimension.height;

        var d = document.getElementById(this.ParentDiv);
        d.style.width = this.ScreenDimension.width + "px";
        d.style.height = this.ScreenDimension.height + "px";

        var leftx = ((window.innerWidth - this.ScreenDimension.width) / 2);
        this.ScreenLocation.left = leftx;
        d.style.left = leftx + "px";
        d.style.top = "0px";

        renderfunction();
    },

    createScreenCanvas: function (parentdiv) {
        this.ParentDiv = parentdiv;

        // create the canvas for the main gamescreen
        var canvas = document.createElement("canvas");
        canvas.id = "gamecanvas";
        canvas.width = this.ScreenDimension.width;
        canvas.height = this.ScreenDimension.height;       
        canvas.style.position = "absolute";

        var d = document.getElementById(parentdiv);
        d.appendChild(canvas);
        d.style.width = this.ScreenDimension.width + "px";
        d.style.height = this.ScreenDimension.height + "px";
        var leftx = ((window.innerWidth - this.ScreenDimension.width) / 2);
        this.ScreenLocation.left = leftx;
        d.style.left = leftx + "px";

        this.ScreenCanvas = canvas;
        this.ScreenContext = canvas.getContext('2d');

        this.ScreenContext.fillStyle = "black";
        this.ScreenContext.fillRect(0, 0, canvas.width, canvas.height);
    },

    screenX: function (x) {
        return (x / this.OriginalDimension.width) * this.ScreenDimension.width;
    },
    screenY: function (y) {
        return (y / this.OriginalDimension.height) * this.ScreenDimension.height;
    },

    originalX: function(x) {
		// converts from screen co-ords to original co-ords
        return (x / this.ScreenDimension.width) * this.OriginalDimension.width;
    },
    originalY: function (y) {
        return (y / this.ScreenDimension.height) * this.OriginalDimension.height;
    },

    getPixelRatio: function () {
        var context = this.ScreenContext;
        var backingStore = context.backingStorePixelRatio ||
              context.webkitBackingStorePixelRatio ||
              context.mozBackingStorePixelRatio ||
              context.msBackingStorePixelRatio ||
              context.oBackingStorePixelRatio ||
              context.backingStorePixelRatio || 1;

        return (window.devicePixelRatio || 1) / backingStore;
    },

    ScaleCanvas: null,
    ScaleContext: null,
    ScaledCanvas: null,

    getImageBetterScaled: function(img) {
        var pixelratio = this.getPixelRatio();

        if (this.ScaleCanvas === null) {
            this.ScaleCanvas = document.createElement('canvas');
            this.ScaledCanvas = document.createElement('canvas'),
            this.ScaleContext = this.ScaleCanvas.getContext('2d');
        }

        // multiply the canvas width to get the non CSS pixel width (real pixels)
        var width = this.screenX(img.width) * pixelratio;
        var height = this.screenY(img.height) * pixelratio;

        // See if we need to do stepped scaling
        var steps = Math.ceil(Math.log(img.width / width) / Math.log(2));
        //console.log("steps: " + steps);
        if (steps < 2) {
            // no need for stepped scaling
            return img;
        }

        // do the stepped scaling
        var i, sw, sh;
        for (i = 1; i < steps; i++) {
            if (i == 1) {
                this.ScaleCanvas.width = img.width * 0.5;
                this.ScaleCanvas.height = img.height * 0.5;
                this.ScaleContext.drawImage(img, 0, 0, this.ScaleCanvas.width, this.ScaleCanvas.height);
                sw = this.ScaleCanvas.width;
                sh = this.ScaleCanvas.height;
            } else {
                if (sw * 0.5 < width || sh * 0.5 < height) break;

                sw = sw * 0.5;
                sh = sh * 0.5;
                this.ScaleContext.drawImage(this.ScaleCanvas, 0, 0, sw, sh);
            }
        }

        var ctx = this.ScaledCanvas.getContext('2d');
        this.ScaledCanvas.width = sw;
        this.ScaledCanvas.height = sh;
        ctx.drawImage(this.ScaleCanvas, 0, 0, sw, sh, 0, 0, sw, sh);
		//console.log("di,oria: "+img.width+" "+img.height+" "+this.getPixelRatio());
		//console.log("di,oria: "+this.screenX(img.width)+" "+this.screenY(img.height)+" "+pixelratio);		
		//console.log("di,orig: "+width+" "+height);		
		//console.log("di,scal: "+sw+" "+sh);		
        return this.ScaledCanvas;
    },

    drawImage: function (img, left, top) {
        var toplot = this.getImageBetterScaled(img);

        this.ScreenContext.drawImage(toplot, this.screenX(left), this.screenY(top), this.screenX(img.width), this.screenY(img.height));
    },

    drawImageDimension: function (img, dimension) {
        var toplot = this.getImageBetterScaled(img);
        this.ScreenContext.drawImage(toplot, this.screenX(dimension.left), this.screenY(dimension.top), this.screenX(dimension.width), this.screenY(dimension.height));
    },

    pointInBox: function(x, y, dimension) {
        if (x >= dimension.left && x <= dimension.left + dimension.width && y >= dimension.top && y <= dimension.top + dimension.height) {
            return true;
        }

        return false;
    },

/*    old_fillRect: function (dimension, add) {
		
        if (add !== undefined) {
            dimension.left -= add;
            dimension.top -= add;
            dimension.width += (add * 2);
            dimension.height += (add * 2);
        }
        this.ScreenContext.fillRect(this.screenX(dimension.left), this.screenY(dimension.top), this.screenX(dimension.width), this.screenY(dimension.height));
    },*/
	
    fillRect: function (left, top, width, height) {
		
		var ctx = this.ScreenContext;
		ctx.fillStyle = this.fillStyle; //style;
		//console.log("fillrect: "+left+" "+top+" "+width+" "+height);
		ctx.fillRect(this.screenX(left), this.screenY(top), this.screenX(width), this.screenY(height));
		

    },	

    strokeRect: function (dimension, add) {
        if (add !== undefined) {
            dimension.left -= add;
            dimension.top -= add;
            dimension.width += (add * 2);
            dimension.height += (add * 2);
        }

        this.ScreenContext.strokeRect(this.screenX(dimension.left), this.screenY(dimension.top), this.screenX(dimension.width), this.screenY(dimension.height));
    },

    roundedRect: function (dimension, radius) {
        var ctx = this.ScreenContext;
        var x = this.screenX(dimension.left);
        var y = this.screenY(dimension.top);
        var width = this.screenX(dimension.width);
        var height = this.screenY(dimension.height);
        

        ctx.beginPath();
        ctx.moveTo(x, y + radius);
        ctx.lineTo(x, y + height - radius);
        ctx.quadraticCurveTo(x, y + height, x + radius, y + height);
        ctx.lineTo(x + width - radius, y + height);
        ctx.quadraticCurveTo(x + width, y + height, x + width, y + height - radius);
        ctx.lineTo(x + width, y + radius);
        ctx.quadraticCurveTo(x + width, y, x + width - radius, y);
        ctx.lineTo(x + radius, y);
        ctx.quadraticCurveTo(x, y, x, y + radius);
        ctx.fill();
        ctx.stroke();
    },

    defaultFont: "andika_basicregular",
	
	setFont: function (font, size) {
		ctx.font = font;
		this.fontSize = size;
	},
	setFont2: function (size, font) { // 16.04 would like to rename this font??
			var getFontSizeScreen = function (size) {
            var xsize = Mag2DScreen.screenX(size);
            var ysize = Mag2DScreen.screenY(size);
            if (xsize > ysize) return xsize; else return ysize;
        }
		var ctx = this.ScreenContext;
		this.font = font;
		// this.fontSize is in the size set by the external class
		// sp translate it into 'real' or 'internal' units
		this.fontSize = size;
		size = getFontSizeScreen(this.fontSize);
		// it actually needs to set the font
		ctx.font = size + "px " + this.defaultFont;
		//console.log("setfont2: "+size + "px " + this.defaultFont);		
	},

	measureText: function (s) {
		var ctx = this.ScreenContext;
		return ctx.measureText(s);
	},
	
	// this returns the text width in "original" units
	textWidth: function (s) {
		var ctx = this.ScreenContext;
		tm = ctx.measureText(s);
		var width = Mag2DScreen.originalX(tm.width);
		return width;
	},	
	
	fillText: function (text, x, y) {
		this.ScreenContext.fillStyle = this.fillStyle; // sometimes colour field is set directly
		this.drawText(text,x , y, this.fontSize);
	},
	
    drawText: function (text, x, y, size, valign, halign) {
		// size is ignored - it's been set in setFont2() and
		// we don't need to pass it in
        /*var getFontSizeScreen = function (size) {
            var xsize = Mag2DScreen.screenX(size);
            var ysize = Mag2DScreen.screenY(size);
            if (xsize > ysize) return xsize; else return ysize;
        }*/
		//console.log("draw text0 "+text+" "+size+" "+x);
        x = this.screenX(x);
        y = this.screenY(y);
		//console.log("draw text1 "+text+" "+size+" "+x);
        //size = getFontSizeScreen(size);
		//console.log("draw text2 "+text+" "+size+" "+x);
        var ctx = this.ScreenContext;

        //ctx.font = size + "px " + this.defaultFont;
        if (valign!==undefined && valign!==null) { ctx.textBaseline = valign; }
        if (halign !== undefined && halign!==null) { ctx.textAlign = halign; }
        ctx.fillText(text, x, y);
    },

    getMouseTouchCoords: function(event) {
        var x = event.layerX;
        var y = event.layerY;
        if (event.offsetX !== undefined) {
            x = event.offsetX;
            y = event.offsetY;
        }
        if (event.targetTouches !== undefined) {
            x = event.targetTouches[0].clientX-this.ScreenLocation.left;
            y = event.targetTouches[0].clientY-this.ScreenLocation.top;
        }

        return { x: x, y: y };
    },

    setClipRegion: function (dimension) {
        var ctx = this.ScreenContext;

        // store state
        ctx.save();

        // create clip region
        ctx.beginPath();
        ctx.lineWidth = 0;
        ctx.strokeStyle = "rgba(0,0,0,0)";
        ctx.shadowBlur = 0;
        ctx.rect(this.screenX(dimension.left), this.screenY(dimension.top), this.screenX(dimension.width), this.screenY(dimension.height));
        ctx.clip();
    },

    unsetClipRegion: function () {
        this.ScreenContext.restore();
    },
	
	moveTo: function (x, y) {
		var ctx = this.ScreenContext;
		
        ctx.moveTo(Mag2DScreen.screenX(x), Mag2DScreen.screenY(y));
    },
	lineTo: function (x, y) {
		var ctx = this.ScreenContext;
		
        ctx.lineTo(Mag2DScreen.screenX(x), Mag2DScreen.screenY(y));
    },
	beginPath: function () {
		var ctx = this.ScreenContext;
		
        ctx.beginPath();
    },
	stroke: function () {
		var ctx = this.ScreenContext;
		
        ctx.stroke();
    },
	
	setStrokeStyle: function (style) {
		var ctx = this.ScreenContext;		
		ctx.strokeStyle = style;
	},
	setFillStyle: function (style) {
		var ctx = this.ScreenContext;	
		this.fillStyle = style;
		ctx.fillStyle = this.fillStyle;
	},	
};