﻿var Mag2DAudio = {
    percloaded: 0,
    numloaded: 0,
    numtoload: 0,

    AudioOptionsSetting: null,

    startLoadingAudio: function (audiolistobject, callback, progress) {
        Mag2DAudio.percloaded = 0;
        Mag2DAudio.numloaded = 0;
        Mag2DAudio.numtoload = 0;

        var checkLoading = function () {
            var nloaded = 0;

            for (var co in audiolistobject) {
                var state = audiolistobject[co].audio.readyState && audiolistobject[co].audio.magmentisready;
                if (state) {
                    nloaded++;
                }
            }

            Mag2DAudio.numloaded = nloaded;
            Mag2DAudio.percloaded = ((Mag2DAudio.numloaded / Mag2DAudio.numtoload) * 100) >>> 0;

            // update progress if a callback has been set
            if (progress !== undefined) {
                progress(Mag2DAudio.percloaded);
            }

            if (nloaded === Mag2DAudio.numtoload) {
                // all loaded
                callback();
            } else {
                setTimeout(checkLoading, 250);
            }
        }

        // get number of audio files to load
        var i;
        for (i in audiolistobject) {
            Mag2DAudio.numtoload++;
        }

        // start the audio loading
        var o;
        for (i in audiolistobject) {
            o = audiolistobject[i];
            o.audio = document.createElement("audio");
            o.audio.oncanplaythrough = function (o) {
                o.audio.magmentisready = 1;
            }(o);
            o.audio.src = o.src;
            o.audio.load();
        }

        setTimeout(checkLoading, 250);
    },

    playAudio: function (audiofile) { // audiofile: { src, translated }
        if (this.AudioOptionsSetting.sound) {
            audiofile.audio.play();
        }

    }
};
